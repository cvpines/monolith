# Monolith

MONOLITH is a bespoke lamp designed and fabricated for a client, featuring 128 RGBW LEDs housed in a handmade Bolivian Rosewood case.

The lamp is capable of displaying solid colors, video playback, and interactive generated images. Rather than the flashy rainbow effects typical of RGB lighting, MONOLITH focuses on warm, gently-evolving color-scapes that mimic candlelight, shimmering water, a forest canopy, and the aurora. Optional WiFi connectivity also allows MONOLITH to display information such as the weather.

A suite of novel lossy and lossless image and video compression codecs designed for low resolution graphics was developed to facilitate high-framerate, low-memory playback. A custom high-quality spatiotemporal dithering algorithm provides 40 bits of color per pixel. The software is designed to be highly fault-tolerant, including robust local and remote debugging and integrated thermal control.