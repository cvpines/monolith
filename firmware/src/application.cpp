#include "modules.h"
#include "application.h"
#include "display.h"
#include "pins.h"
#include "neopixel.h"
#include "encoder.h"
#include "tsense.h"
#include "faults.h"
#if ACCEL_EN
#include "accel.h"
#endif
#if DEBUG_EN
#include "debug.h"
#endif

SYSTEM_MODE(MANUAL);

void setup(void) {
	setup_pins();

	Encoder.init();
    TSense.init();
	Faults.init();
	#if ACCEL_EN
    Accel.init();
	#endif
	#if DEBUG_EN
	Debug.init();
	#endif
	Display.init();
 
}


void loop(void) {
	Encoder.update();
	Display.update();
	Faults.update();
	Debug.update();

	delay(5);
}
