//
// Created by Coriander V. Pines on 2017/03/28.
//

#include "program.h"

#include "modules.h"
#include "display.h"
#include "encoder.h"

#include "neopixel.h"


uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return Adafruit_NeoPixel::Color(255 - WheelPos * 3, 0, WheelPos * 3, 0);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return Adafruit_NeoPixel::Color(0, WheelPos * 3, (255 - WheelPos * 3), 0);
  }
  WheelPos -= 170;
  return Adafruit_NeoPixel::Color(WheelPos * 3, (255 - WheelPos * 3), 0,0);
}


void ProgramFlameVideo::loop(unsigned long time) {
	if (player.update(time)) {
    	Display.blitImage(player.getCurrentFrame());
  	}
}

void ProgramSolidWhite::enter(unsigned long time) {
	// 170 limit helps control temperature output (seems to be a fixed point around 41C)
	Display.clear(0, 0, 0, 170);
}


void ProgramPalette::enter(unsigned long time) {
	param_change(time);
}

void ProgramPalette::param_change(unsigned long time) {
	uint32_t color;
	if (wrap) {
		value = (value + Encoder.position) % 256;
		color = indexPaletteUniform16Wrap(palette, value);
	} else {
		value = min(max(value + Encoder.position, 0), 255);
		color = indexPaletteUniform16(palette, value);
	}
	Encoder.reset_position();


	Display.clear(color);
}

#if ACCEL_EN
#include "accel.h"
void ProgramAccel::enter(unsigned long time) {
	Display.clear(0, 255, 0, 0);
}

void ProgramAccel::loop(unsigned long time) {

}
#endif