#include "encoder.h"

#include "pins.h"
#include "application.h"

EncoderClass Encoder;

void EncoderClass::init() {
    attachInterrupt(PIN_ENC_A, enc_a_isr, CHANGE);
    attachInterrupt(PIN_ENC_B, enc_b_isr, CHANGE);
}

void EncoderClass::update(void) {
    bool new_state =  digitalRead(PIN_ENC_S) ^ ENC_S_INVERT;

    // Rising edge
    if (new_state && !button_state) {
        button_pressed_flag = true;
        button_released_flag = false;
        button_time = millis();
    }

    // Falling edge
    if (!new_state && button_state) {
        button_pressed_flag = false;
        button_released_flag = true;
    }

    button_state = new_state;
}

bool EncoderClass::button_pressed(bool clear_flag) {
    bool ret = button_pressed_flag;
    if (clear_flag) button_pressed_flag = false;
    return ret;
}

unsigned long EncoderClass::button_held(void) {
    if (button_state) {
        return millis() - button_time;
    } else {
        return 0;
    }
}

unsigned long EncoderClass::button_released(bool clear_flag) {
    bool ret = button_released_flag;
    if (clear_flag) button_released_flag = false;
    if (ret) {
        return millis() - button_time;
    } else {
        return 0;
    }
}

bool EncoderClass::position_changed(void) {
    bool ret = pos_changed_flag;
    pos_changed_flag = false;
    return ret;
}

void EncoderClass::reset_position(void) {
	position = 0;
}

void enc_a_isr(void) {
    Encoder.val_a = digitalRead(PIN_ENC_A);
    Encoder.position += (Encoder.val_a == Encoder.val_b) ? -1 : +1;
    Encoder.pos_changed_flag = true;
}

void enc_b_isr(void) {
    Encoder.val_b = digitalRead(PIN_ENC_B);
    Encoder.position += (Encoder.val_a == Encoder.val_b) ? +1 : -1;
    Encoder.pos_changed_flag = true;
}