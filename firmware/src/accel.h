#ifndef ACCEL_H
#define ACCEL_H

#include "application.h"

class AccelClass {
public:
	static const int ACCEL_INTERVAL_MS = 30;
	static const int ACCEL_MULTISAMPLE = 4;

    int x, y, z;
    int up_axis;
    int u, v;

    AccelClass(void);

    void init(void);

private:
    Timer accel_timer;
    friend void accel_cb(void);
};

void accel_cb(void);

extern AccelClass Accel;

#endif //ACCEL_H
