//
// Created by Coriander V. Pines on 2017/03/19.
//

#include "tsense.h"

#include "faults.h"
#include "pins.h"

// 1.0 - exp(x) from -5 to 0
static const int TEMP_CONTROL_CURVE[] = {
    254, 254, 253, 253, 253, 253, 253, 253, 253, 253,
    253, 252, 252, 252, 252, 252, 251, 251, 251, 251, 
    251, 250, 250, 250, 249, 249, 249, 249, 248, 248, 
    247, 247, 247, 246, 246, 245, 245, 244, 243, 243, 
    242, 241, 241, 240, 239, 238, 237, 236, 236, 234, 
    233, 232, 231, 230, 229, 227, 226, 224, 223, 221, 
    219, 217, 215, 213, 211, 209, 206, 204, 201, 198, 
    195, 192, 189, 186, 182, 178, 174, 170, 166, 161, 
    156, 151, 146, 140, 135, 128, 122, 115, 108, 100, 
     92,  84,  75,  66,  56,  46,  35,  24,  12,   6
};
static const int TEMP_CONTROL_CURVE_LEN = 100;

TSenseClass TSense;

TSenseClass::TSenseClass(void) : update_timer(TSenseClass::TSENSE_INTERVAL_MS, tsense_cb) {

}

void TSenseClass::init(void) {
    update_timer.start();
}

void TSenseClass::update(void) {

}

void tsense_cb(void) {
    // Get raw counts
    int brd = analogRead(PIN_TSENSE_BRD); // 12-bit = [0, 4095]
    int led = analogRead(PIN_TSENSE_LED);

    // Temp in C = ADC counts * (3300/4095) mV/count * 0.1 C/mV - 50 C
    // The TMP36 has 0.1 C precision, so we use a fixed-point representation
    TSense.brd_tenth_c = (3300 * brd) / 4095 - 500;
    TSense.led_tenth_c = (3300 * led) / 4095 - 500;

    // Check for suspiciously-low temperatures that might indicate invalid data
    // Making a conscious decision here to keep the flag set if the fault is resolved. Better play it safe.
    if (TSense.brd_tenth_c < TSense.TSENSE_INVALID_MIN_TENTH_C) {
        Faults.setFault(FAULT_TSENSE_BRD_INVALID);
    }
    if (TSense.led_tenth_c < TSense.TSENSE_INVALID_MIN_TENTH_C) {
        Faults.setFault(FAULT_TSENSE_LED_INVALID);
    }

    // Disable screen if we can't get a reading.
    if (Faults.checkFault(FAULT_TSENSE_INVALID)) {
        TSense.brightness_limit = 0;
        return;
    }

    // Check for temperature faults and set brightness limit
    int _brightness_limit = 255;

    // Calculate limit from board temp
    if (TSense.brd_tenth_c < TSense.TSENSE_BRD_CURVE_MIN_TENTH_C) {
        Faults.clearFault(FAULT_TSENSE_BRD_SOFT_LIMIT | FAULT_TSENSE_BRD_HARD_LIMIT);
        _brightness_limit = 255;
    } else if (TSense.brd_tenth_c < TSense.TSENSE_BRD_HARD_MAX_TENTH_C) {
        Faults.setFault(FAULT_TSENSE_BRD_SOFT_LIMIT);
        Faults.clearFault(FAULT_TSENSE_BRD_HARD_LIMIT);

        // Interpolate along temp curve
        int curve_index = (TEMP_CONTROL_CURVE_LEN * (TSense.brd_tenth_c - TSense.TSENSE_BRD_CURVE_MIN_TENTH_C)) / (TSense.TSENSE_BRD_HARD_MAX_TENTH_C - TSense.TSENSE_BRD_CURVE_MIN_TENTH_C);
        curve_index = (curve_index < TEMP_CONTROL_CURVE_LEN) ? (curve_index > 0) ? curve_index : 0 : TEMP_CONTROL_CURVE_LEN;
        _brightness_limit = TEMP_CONTROL_CURVE[curve_index];
    } else {
        Faults.setFault(FAULT_TSENSE_BRD_HARD_LIMIT);
        Faults.clearFault(FAULT_TSENSE_BRD_SOFT_LIMIT);
        _brightness_limit = 0;
    }

    // LED Panel
    if (TSense.led_tenth_c < TSense.TSENSE_LED_CURVE_MIN_TENTH_C) {
        Faults.clearFault(FAULT_TSENSE_LED_SOFT_LIMIT | FAULT_TSENSE_LED_HARD_LIMIT);
        _brightness_limit = min(_brightness_limit, 255);
    } else if (TSense.led_tenth_c < TSense.TSENSE_LED_HARD_MAX_TENTH_C) {
        Faults.setFault(FAULT_TSENSE_LED_SOFT_LIMIT);
        Faults.clearFault(FAULT_TSENSE_LED_HARD_LIMIT);

        // Interpolate along temp curve
        int curve_index = (TEMP_CONTROL_CURVE_LEN * (TSense.led_tenth_c - TSense.TSENSE_LED_CURVE_MIN_TENTH_C)) / (TSense.TSENSE_LED_HARD_MAX_TENTH_C - TSense.TSENSE_LED_CURVE_MIN_TENTH_C);
        curve_index = (curve_index < TEMP_CONTROL_CURVE_LEN) ? (curve_index > 0) ? curve_index : 0 : TEMP_CONTROL_CURVE_LEN;
        _brightness_limit = min(_brightness_limit, TEMP_CONTROL_CURVE[curve_index]);
    } else {
        Faults.setFault(FAULT_TSENSE_LED_HARD_LIMIT);
        Faults.clearFault(FAULT_TSENSE_LED_SOFT_LIMIT);
        _brightness_limit = 0;
    }

    // Prevent race conditions since we run in a seperate thread
    TSense.brightness_limit = _brightness_limit;
}

/*
 * dT = a * (Tamb - T) + bright * C
 * Try something like 1 - exp as it gets closer?
 */