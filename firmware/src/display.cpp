//
// Created by Coriander V. Pines on 2017/03/19.
//

#include "display.h"

#include "encoder.h"
#include "tsense.h"
#include "pins.h"
#include "program.h"
#include "faults.h"
#include "gamma.h"
#include "palette.h"

#include "puff.h" // Deflate


// Program array
Program* const PROGRAM_ARRAY[] = {
		new ProgramSolidWhite(),
		new ProgramPalette(PaletteParty, true),
		//new ProgramPalette(PaletteCandleRGB, false),
		//new ProgramPalette(PaletteCandleRGBW, false),
		//new ProgramPalette(PaletteCloud, true),
		//new ProgramPalette(PaletteOcean, true),
		//new ProgramPalette(PaletteLava, true),
		//new ProgramPalette(PaletteForest, true),
		//new ProgramPalette(PaletteBlackbody, false),
		new ProgramFlameVideo(),
#if ACCEL_EN
		new ProgramAccel(),
#endif
		// Terminate with a nullptr to avoid dynamic array size woes
		nullptr
};


// Display singleton
DisplayClass Display;


/* PUBLIC */

DisplayClass::DisplayClass(void) : matrix(DisplayClass::WIDTH * DisplayClass::HEIGHT, PIN_MATRIX, SK6812GRBW) {
	clear();
}


// Initialize
void DisplayClass::init(void) {
	matrix.begin();
	updateBrightness();
	PROGRAM_ARRAY[program_idx]->enter(0);
}


// Update per frame. Runs current program and handles UI
void DisplayClass::update(void) {
	unsigned long time = millis();

	if (param_set) {
		// Param mode

		// Send parameter changes
		if (Encoder.position_changed()) {
			param_start_time = time;
			PROGRAM_ARRAY[program_idx]->param_change(time);
		}

		// Change mode
		if (Encoder.button_pressed()) {
			PROGRAM_ARRAY[program_idx]->exit(time);
			program_idx++;
			if (PROGRAM_ARRAY[program_idx] == nullptr) {
				program_idx = 0;
			}
			PROGRAM_ARRAY[program_idx]->enter(time);
			param_start_time = time;
		}

		// Check for exit
		if (time - param_start_time > PARAM_SET_DURATION_MS) {
			param_set = false;
			Encoder.reset_position();
		}
	} else {
		// Brightness mode

		// Button press enters param mode
		if (Encoder.button_pressed()) {
			param_set = true;
			param_start_time = time;
			Encoder.reset_position();
		}

		// Encoder turn changes brightness
		if (Encoder.position_changed()) {
			updateBrightness();
		}
	}

	// Check for sleep mode
	if (Encoder.button_held() > SLEEP_HOLD_DURATION_MS) {
		enterSleepMode();
	}

	// Enforce thermal brightness limit
	if (TSense.brightness_limit < brightness) brightness = TSense.brightness_limit;

	// Execute the program
	PROGRAM_ARRAY[program_idx]->loop(time);

	// Refresh display
	show(time);
}


// Put the device into sleep mode
void DisplayClass::enterSleepMode(void) {
	matrix.clear();
	matrix.show();
	// "Soft" sleep mode - Reenters when woken up
	// TODO: Save persistent settings here
	// TODO: I don't like using ENC_A to take it out of sleep, and since
	// ENC_S isn't available for wakeup, might want to change this to a loop + interrupt
	// TODO: How does this work with the watch dog?
	System.sleep(PIN_ENC_A, CHANGE, 0);
}


// Reset the buffer to black
void DisplayClass::clear(void) {
	for (size_t i = 0; i < WIDTH * HEIGHT * CHANNELS; i++) {
		buffer[i] = 0;
	}
}


// Clear the whole buffer
void DisplayClass::clear(uint8_t r, uint8_t g, uint8_t b, uint8_t w) {
	size_t buffer_idx = 0;
	for (int idx = 0; idx < WIDTH*HEIGHT; idx++) {
		buffer[buffer_idx++] = r;
		buffer[buffer_idx++] = g;
		buffer[buffer_idx++] = b;
		buffer[buffer_idx++] = w;
	}
}


// Clear the whole buffer in the form WWRRGGBB
void DisplayClass::clear(uint32_t color) {
	uint8_t w = (color >> 24) & 0xFF;
	uint8_t r = (color >> 16) & 0xFF;
	uint8_t g = (color >>  8) & 0xFF;
	uint8_t b = (color      ) & 0xFF;
	clear(r, g, b, w);
}


// Set a pixel in the buffer
void DisplayClass::setPixel(unsigned int x, unsigned y, uint8_t r, uint8_t g, uint8_t b, uint8_t w) {
	if ((x >= 0) && (x < WIDTH) && (y >= 0) && (y < HEIGHT)) {
		size_t idx = (x + y * WIDTH) * CHANNELS;
		buffer[idx    ] = r;
		buffer[idx + 1] = g;
		buffer[idx + 2] = b;
		buffer[idx + 3] = w;
	}
}


// Set a pixel in the format WWRRGGBB
void DisplayClass::setPixel(unsigned int x, unsigned y, uint32_t color) {
	uint8_t w = (color >> 24) & 0xFF;
	uint8_t r = (color >> 16) & 0xFF;
	uint8_t g = (color >>  8) & 0xFF;
	uint8_t b = (color      ) & 0xFF;
	setPixel(x, y, r, g, b, w);
}


// Blit an image to the buffer
int DisplayClass::blitImage(const uint8_t* image, int x, int y) {
	if (!image) {
		Faults.setFault(FAULT_IMAGE_INVALID);
		return -1;
	}

	// Parse header
	size_t idx = 0;
	uint8_t width = image[idx++];
	uint8_t height = image[idx++];
	uint8_t compression = image[idx++];
	idx++;
	idx++; // Reserved
	size_t data_size = image[idx++] << 8; // Size HI
	data_size |= image[idx++]; // Size LO
	const uint8_t *data = &image[idx++];

	// Skip totally out-of-bounds images (prevents an infinite loop in the blit code!)
	if ((x >= WIDTH) || (x + width <= 0) ||
		(y >= HEIGHT) || (y + height <= 0)) {
		return -1;
	}

	// Defer by compression type
	if (compression == IMAGE_UNCOMPRESSED_FULL) {
		// Load full color image data
		return blitFullColor(data_size, data, width, height, x, y);
	} else if (compression == IMAGE_DEFLATE_FULL) {
		// Prepare a temp buffer to store deflated data
		// CHANNELS bytes per pixel
		unsigned long destlen = width * height * CHANNELS;
		unsigned long srclen = data_size;
		uint8_t *buffer = new uint8_t[destlen];

		// INFLATE stored data
		int ret = puff(buffer, &destlen, data, &srclen);
		if (ret != 0) {
			Faults.setFault(FAULT_IMAGE_DECOMPRESS);
			delete[] buffer;
			buffer = nullptr;
			return ret;
		}

		// Load full color image data
		int ret2 = blitFullColor(destlen, buffer, width, height, x, y);

		// Free the temp buffer
		delete[] buffer;
		buffer = nullptr;

		return ret2;
	} else if (compression == IMAGE_UNCOMPRESSED_MONOCHROME) {
		// Load monochrome image data
		return blitMonochrome(data_size, data, width, height, x, y);
	} else if (compression == IMAGE_DEFLATE_MONOCHROME) {
		// Prepare a temp buffer to store deflated data
		// CHANNELS bytes for cast color + 1 byte per pixel
		unsigned long destlen = Display.CHANNELS + width * height;
		unsigned long srclen = data_size;
		uint8_t *buffer = new uint8_t[destlen];

		// INFLATE stored data
		int ret = puff(buffer, &destlen, data, &srclen);
		if (ret != 0) {
			Faults.setFault(FAULT_IMAGE_DECOMPRESS);
			delete[] buffer;
			buffer = nullptr;
			return ret;
		}

		// Load monochrome image data
		int ret2 = blitMonochrome(destlen, buffer, width, height, x, y);

		// Free the temp buffer
		delete[] buffer;
		buffer = nullptr;

		return ret2;
	} else if (compression == IMAGE_UNCOMPRESSED_PALETTE) {
		return blitPalette(data_size, data, width, height, x, y);
	} else if (compression == IMAGE_DEFLATE_PALETTE) {
		// Prepare a temp buffer to store deflated data
		// 16 * CHANNELS bytes for palette + 1 byte per pixel
		unsigned long destlen = 16 * Display.CHANNELS + width * height;
		unsigned long srclen = data_size;
		uint8_t *buffer = new uint8_t[destlen];

		// INFLATE stored data
		int ret = puff(buffer, &destlen, data, &srclen);
		if (ret != 0) {
			Faults.setFault(FAULT_IMAGE_DECOMPRESS);
			delete[] buffer;
			buffer = nullptr;
			return ret;
		}

		// Load monochrome image data
		int ret2 = blitPalette(destlen, buffer, width, height, x, y);

		// Free the temp buffer
		delete[] buffer;
		buffer = nullptr;

		return ret2;
	} else {
		// Invalid compression
		Faults.setFault(FAULT_IMAGE_INVALID);
		return -1;
	}
}


/* PRIVATE */

// Intermediate for blitting full color images
int DisplayClass::blitFullColor(size_t data_size, const uint8_t* data, uint8_t width, uint8_t height, int x, int y) {
	// Check that size is valid
	if (data_size != (width * height * CHANNELS)) {
		Faults.setFault(FAULT_IMAGE_INVALID);
		return -1;
	}

	// Blit the visible portion to the matrix buffer
	uint8_t red, green, blue, white;
	const uint8_t* pixel_ptr;
	for (int j = max(0, -y); j < min(height, HEIGHT - y); j++) {
		for (int i = max(0, -x); i < min(width, WIDTH - x); i++) {
			// Unpack pixels in groups of CHANNELS
			pixel_ptr = &data[CHANNELS * (i + j * width)];
			red = *pixel_ptr++;
			green = *pixel_ptr++;
			blue = *pixel_ptr++;
			white = *pixel_ptr;

			if (true) { // TODO: Masking
				setPixel(x + i, y + j,
						 red, green, blue, white);
			}
		}
	}
	return 0;
}


// Intermediate for blitting monochrome images
int DisplayClass::blitMonochrome(size_t data_size, const uint8_t* data, uint8_t width, uint8_t height, int x, int y) {
	// Check that size is valid
	if (data_size != (CHANNELS + width * height)) {
		Faults.setFault(FAULT_IMAGE_INVALID);
		return -1;
	}

	// Decode cast color
	size_t idx = 0;
	uint8_t red = data[idx++];
	uint8_t green = data[idx++];
	uint8_t blue = data[idx++];
	uint8_t white = data[idx++];

	// Blit the visible portion to the matrix buffer
	uint8_t pixel = 0;
	for (int j = max(0, -y); j < min(height, HEIGHT - y); j++) {
		for (int i = max(0, -x); i < min(width, WIDTH - x); i++) {
			pixel = data[idx + i + j * width];

			if (true) { // TODO: Masking
				setPixel(x + i, y + j,
						 pixel * red / 255,
						 pixel * green / 255,
						 pixel * blue / 255,
						 pixel * white / 255);
			}
		}
	}

	return 0;
}


// Intermediate for blitting paletted images
int DisplayClass::blitPalette(size_t data_size, const uint8_t* data, uint8_t width, uint8_t height, int x, int y) {
	// Check that size is valid
	if (data_size != (16 * CHANNELS + width * height)) {
		Faults.setFault(FAULT_IMAGE_INVALID);
		return -1;
	}

	// Decode palette
	size_t idx = 0;
	const uint8_t* palette = &data[idx];
	idx += 64; // 64 bytes big

	// Blit the visible portion to the matrix buffer
	uint8_t pixel = 0;
	for (int j = max(0, -y); j < min(height, HEIGHT - y); j++) {
		for (int i = max(0, -x); i < min(width, WIDTH - x); i++) {
			pixel = data[idx + i + j * width];

			if (true) { // TODO: Masking
				// Look the color up in the palette
				setPixel(x + i, y + j, indexPaletteUniform16(palette, pixel));
			}
		}
	}

	return 0;
}


// For internal system use only. Displays an image directly to the matrix for overlays
// IMPORTANT: This is not modulated by dynamic brightness. For this reason, channel brightness is capped
// Only supports IMAGE_UNCOMPRESSED_FULL
int DisplayClass::directBlitRawImage(const uint8_t* image, int x, int y) {
	static const uint8_t max_bright = 3; // of 255

	if (!image) {
		Faults.setFault(FAULT_IMAGE_INVALID);
		return -1;
	}

	// Parse header
	size_t idx = 0;
	uint8_t width = image[idx++];
	uint8_t height = image[idx++];
	uint8_t compression = image[idx++];
	idx++; idx++; // Reserved
	size_t data_size = image[idx++] << 8; // Size HI
	data_size |= image[idx++]; // Size LO
	const uint8_t * data = &image[idx++];

	// Skip totally out-of-bounds images (prevents an infinite loop in the blit code!)
	if ((x >= WIDTH) || (x + width <= 0) ||
		(y >= HEIGHT) || (y + height <= 0)) {
		return -1;
	}

	// Only supports IMAGE_UNCOMPRESSED_FULL
	if (compression == IMAGE_UNCOMPRESSED_FULL) {
		// Load full color image data
		// Check that size is valid
		if (data_size != (width * height * CHANNELS)) {
			Faults.setFault(FAULT_IMAGE_INVALID);
			return -1;
		}

		// Blit the visible portion to the matrix buffer
		uint8_t red, green, blue, white;
		const uint8_t* pixel_ptr;
		for (int j = max(0, -y); j < min(height, HEIGHT - y); j++) {
			for (int i = max(0, -x); i < min(width, WIDTH - x); i++) {
				// Unpack pixels in groups of CHANNELS
				pixel_ptr = &data[CHANNELS * (i + j * width)];
				red = *pixel_ptr++;
				green = *pixel_ptr++;
				blue = *pixel_ptr++;
				white = *pixel_ptr;

				uint32_t color = (white << 24) | (red << 16) | (green << 8) | (blue);
				if (color != IMAGE_COLOR_MASK) {
					matrix.setPixelColor((x + i) + (y + j) * WIDTH,
										 min(red, max_bright),
										 min(green, max_bright),
										 min(blue, max_bright),
										 min(white, max_bright));
				}
			}
		}
		return 0;
	} else {
		// Invalid compression
		Faults.setFault(FAULT_IMAGE_INVALID);
		return -1;
	}
}


// Draw overlay on top of buffer
void DisplayClass::overlay(unsigned long time) {
	// Draw param mode time bar
	if (param_set) {
		int remaining = PARAM_SET_DURATION_MS + param_start_time - time;
		for (unsigned int i = 0; i < WIDTH; i++) {
			matrix.setPixelColor(WIDTH * (HEIGHT - 1) + i, PARAM_SET_BAR_COLOR);
			// Each pixel represents PARAM_SET_BAR_STEP seconds of remaining time
			remaining -= PARAM_SET_BAR_STEP;
			if (remaining <= 0) break;
		}
	}

	// Display thermometer during thermal overload or invalid
	if (Faults.checkFault(FAULT_TSENSE_HARD_LIMIT)) {
		directBlitRawImage(ui_thermal_over_data, 1, 1);
	}
	if (Faults.checkFault(FAULT_TSENSE_INVALID)) {
		directBlitRawImage(ui_thermal_invalid_data, 1, 1);
	}


	// TODO: Additional warnings
	// Possibly wifi status too.
};


// display contents of the buffer on the matrix
void DisplayClass::show(unsigned long time) {
	// Copy buffer to matrix, applying adjustments and dithering
	size_t matrix_idx = 0;
	size_t buffer_idx = 0;
	for (int y = 0; y < HEIGHT; y++) {
		for (int x = 0; x < WIDTH; x++) {
			// Spatial dither
			bool dither_offset = (x + y) & 0b1;

			matrix.setPixelColor(matrix_idx++,
								 adjustColor(buffer[buffer_idx    ], dither_offset),
								 adjustColor(buffer[buffer_idx + 1], dither_offset),
								 adjustColor(buffer[buffer_idx + 2], dither_offset),
								 adjustColor(buffer[buffer_idx + 3], dither_offset));
			buffer_idx += CHANNELS;
		}
	}

	// Draw overlay items
	overlay(time);

	// Send matrix data to hardware
	matrix.show();

	// Advance temporal dither counter
	dither_counter++;
	dither_counter &= (1 << DITHER_BITS) - 1;
}


// Apply gamma curve, spatial + temporal dithering to a single-channel value
inline uint8_t DisplayClass::adjustColor(uint8_t color, bool dither_offset) {
	// Apply global brightness, 2.2 gamma adjustment, and 1-bit temporal dither
	// Use brightness dither to gain an extra bit of precision
	uint16_t brightness9 = (brightness << 1) | brightness_dither;

	// Apply gamma table, using extra bits for temporal dithering
	uint16_t table9 = gamma_10_to_9[(2 * color * brightness9) / 255];
	uint8_t value8 = table9 >> 1;
	uint8_t value1 = table9 & 0b1;

	// Apply temporal dither (Don't bother if we're at max already)
	if (value8 < 255) {
		return value8 + (((value1 > (dither_counter != dither_offset))) ? 1 : 0);
	} else {
		return value8;
	}
}


// Called when the knob is turned in brightness mode
void DisplayClass::updateBrightness(void) {
	// Get change in encoder position. We take full control of the encoder here
	// since nothing else is using it in brightness mode.
	int enc = Encoder.position;
	Encoder.reset_position();

	// Calculate the dithered brightness delta using fixed point math.
	int delta = (BRIGHTNESS_NUMERATOR * enc + brightness_error) / BRIGHTNESS_DENOMINATOR;
	brightness += delta;

	// Brightness error is in fractions of BRIGHTNESS_DENOMINATOR
	brightness_error += BRIGHTNESS_NUMERATOR * enc - BRIGHTNESS_DENOMINATOR * delta;

	// Set dither flag (9th bit of brightness)
	brightness_dither = (2 * ((brightness_error > 0) ? brightness_error : -brightness_error) > BRIGHTNESS_DENOMINATOR);

	// Clamp
	if (brightness < GLOBAL_BRIGHTNESS_MIN) {
		brightness = GLOBAL_BRIGHTNESS_MIN;
		brightness_error = 0;
		brightness_dither = false;
	}
	if (brightness > GLOBAL_BRIGHTNESS_MAX) {
		brightness = GLOBAL_BRIGHTNESS_MAX;
		brightness_error = 0;
		brightness_dither = false;
	}

}
