//
// Created by Coriander V. Pines on 2017/03/28.
//

#ifndef PROGRAM_H
#define PROGRAM_H

#include "modules.h"
#include "videoplayer.h"
#include "video_flame.h"
#include "palette.h"

// Base class for display programs
// Inherit from this class and extend methods where necessary.
class Program {
public:
	// Called when the mode is entered
	virtual void enter(unsigned long time) {};

	// Called just before the mode is left
	virtual void exit(unsigned long time) {};

	// Called when the param is changed
	virtual void param_change(unsigned long time) {};

	// Called once per loop
	virtual void loop(unsigned long time) {};
};


// Monochrome flame video
class ProgramFlameVideo : public Program {
public:

	ProgramFlameVideo(void) : player(video_flame_data) {}
	void loop(unsigned long time);

private:
	VideoPlayer player;
};


// Solid white
class ProgramSolidWhite : public Program {
	void enter(unsigned long time);
};



// Palettes
class ProgramPalette: public Program {
public:
	ProgramPalette(const PaletteUniform16_t pal, bool wrap = false) :
			palette(pal), value(0), wrap(wrap) {};

	void enter(unsigned long time);
	void param_change(unsigned long time);

private:
	const uint8_t * palette;
	int value;
	bool wrap;
};



#if ACCEL_EN
class ProgramAccel : public Program {
public:
	void enter(unsigned long time);
	void loop(unsigned long time);
};
#endif

#endif //PROGRAM_H

/*
 * Program ideas:
 * Slow pastel fade
 * Gradient players
 * Fill-up
 * Landscape + sun
 * Time of day
 * Weather
 * Temperature
 */