//
// Created by Coriander V. Pines on 2017/03/19.
//

#include "faults.h"

#include "modules.h"
#include "display.h"

FaultHandler Faults;

FaultHandler::FaultHandler(void) : watchdog(FaultHandler::WATCHDOG_TIMEOUT_MS, watchdog_cb) {

}

void FaultHandler::init(void) {

}

void FaultHandler::update(void) {
	watchdog.checkin();
}

void FaultHandler::setFault(uint32_t fault) {
	faults |= fault;
};

void FaultHandler::clearFault(uint32_t fault) {
	faults &= ~fault;
};

uint32_t FaultHandler::getFaults(void) {
	return faults;
}

bool FaultHandler::checkFault(uint32_t fault) {
	return faults & fault;
}

void FaultHandler::clearDisplay(void) {
	Display.matrix.clear();
	Display.matrix.show();
}

void watchdog_cb() {
	#if DEBUG_EN
	Serial.println("FAULT\nfaults/msg=Watchdog timeout\n---");
	Serial.flush();
	#endif
	Faults.clearDisplay();
	Faults.setFault(FAULT_WATCHDOG_TIMEOUT);
	// TODO: Save faults to Eeprom
	System.reset(RESET_REASON_WATCHDOG);
}
