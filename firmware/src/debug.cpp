#include "debug.h"

#include "modules.h"
#include "encoder.h"
#include "display.h"
#include "tsense.h"
#include "faults.h"
#if ACCEL_EN
#include "accel.h"
#endif

DebugClass Debug;

DebugClass::DebugClass(void) : status_timer(DebugClass::STATUS_INTERVAL_MS, status_cb) {

}


void DebugClass::init(void) {
    status_timer.start();
}

void DebugClass::update(void) {
    processSerial();
}

void DebugClass::processSerial(void) {
    /*
    Commands:
    a - Arm advanced command
    f - Enter DFU flash mode (arm)
    r - Reset processor (arm)


    Responses:
    ? - Unknown command
    y - Achnowledge
    n - Failure
    */
    while (Serial.available()) {
        char cmd = Serial.read();
        switch (cmd) {
            case 'a':
                advanced_cmd_armed = true;
                Serial.write('y');
                break;
			case 's':
				if (advanced_cmd_armed) {
					advanced_cmd_armed = false;
					Serial.write('y');
					Serial.flush();
					slaveLoop();
				} else {
					Serial.write('n');
				}
				break;
			case 'f':
                if (advanced_cmd_armed) {
                    advanced_cmd_armed = false;
                    Serial.write('y');
                    Serial.flush();
                    System.dfu(false);
                } else {
                    Serial.write('n');
                }
                break;
            case 'r':
                if (advanced_cmd_armed) {
                    advanced_cmd_armed = false;
                    Serial.write('y');
                    Serial.flush();
                    System.reset(RESET_REASON_USER);
                } else {
                    Serial.write('n');
                }
                break;
            default:
                advanced_cmd_armed = false;
                Serial.write('?');
                break;
        }
    }
}

void DebugClass::reportConnect(void) {
    Serial.print("CONNECT\n");
    Serial.print("debug/build=" __DATE__ " " __TIME__ "\n");
	Serial.print("debug/firmware=");
	Serial.println(System.version());
    Serial.printf("debug/reset=%i/%i\n", System.resetReason(), System.resetReasonData());
    Serial.printf("debug/interval=%i\n", STATUS_INTERVAL_MS);
	Serial.printf("debug/init_free=%i\n", System.freeMemory());
	Serial.printf("faults/watchdog_timeout=%i\n", Faults.WATCHDOG_TIMEOUT_MS);
    Serial.printf("tsense/interval=%i\n", TSense.TSENSE_INTERVAL_MS);
    Serial.printf("tsense/brd_min=%d\tbrd_max=%d\n", TSense.TSENSE_BRD_CURVE_MIN_TENTH_C, TSense.TSENSE_BRD_HARD_MAX_TENTH_C);
    Serial.printf("tsense/led_min=%d\tled_max=%d\n", TSense.TSENSE_LED_CURVE_MIN_TENTH_C, TSense.TSENSE_LED_HARD_MAX_TENTH_C);
	#if ACCEL_EN
    Serial.printf("accel/interval=%i\tmulti=%i\n", Accel.ACCEL_INTERVAL_MS, Accel.ACCEL_MULTISAMPLE);
	#endif

    Serial.print("debug/modules=");
	#if ACCEL_EN
    Serial.print("ACCEL ");
	#endif
	#if LUX_EN
    Serial.print("LUX ");
	#endif
	#if ISENSE_EN
    Serial.print("ISENSE ");
	#endif
	#if NET_EN
    Serial.print("NET ");
	#endif
    Serial.print("TSENSE FAULTS ENCODER DISPLAY\n");
    Serial.print("---\n");
	Serial.flush();
}

void DebugClass::reportStatus(void) {
    Serial.print("STATUS\n");
    register char * stack_pointer asm ("sp");
	Serial.printf("debug/free=%d\tstack=%d\n", System.freeMemory(), (size_t)stack_pointer);
    Serial.printf("tsense/brd=%d\tled=%d\n", TSense.brd_tenth_c, TSense.led_tenth_c);
    Serial.printf("tsense/limit=%d\n", TSense.brightness_limit);
    Serial.printf("encoder/pos=%d\tbtn=%d\n", Encoder.position, Encoder.button_held());
    Serial.printf("display/bright=%d\terr=%d\tdither=%d\n", Display.brightness, Display.brightness_error, Display.brightness_dither);
    Serial.printf("display/program_idx=%d\n", Display.program_idx);
    Serial.printf("display/param=%d\n", Display.param_set);
	Serial.printf("faults/flags=%d\n", Faults.getFaults());
	Serial.printf("faults/watchdog_remaining=%i\n", Faults.watchdog.ms_until_timeout());
	Serial.printf("display/gamma_bright=%i\n", Display.adjustColor(255));
	#if ACCEL_EN
    Serial.printf("accel/x=%d\ty=%d\tz=%d\n", Accel.x, Accel.y, Accel.z);
    Serial.printf("accel/up=%d\tu=%d\tv=%d\n", Accel.up_axis, Accel.u, Accel.v);
	#endif
    Serial.print("---\n");
	Serial.flush();
}

void DebugClass::slaveLoop(void) {
	bool do_loop = true;
	while (do_loop) {
		if (Serial.available() > 1) {
			// Read in mode
			int mode = Serial.read();
			switch (mode) {
				case 0x00: // Raw transfer
				{
					// Accept one quartet per pixel
					unsigned long start_time = millis();
					for (unsigned int n = 0; n < Display.WIDTH * Display.HEIGHT; n++) {
						// Wait for a full quartet (no timeout because this is an
						// internal debug tool and the watchdog will catch it
						while (Serial.available() < 4) {
							if (millis() - start_time > SLAVE_LOOP_TIMEOUT_MS) {
								do_loop = false;
								break;
							}
						}
						uint8_t red = Serial.read();
						uint8_t green = Serial.read();
						uint8_t blue = Serial.read();
						uint8_t white = Serial.read();
						Display.matrix.setPixelColor(n, red, green, blue, white);
					}
					Display.matrix.show();
				}
					break;
				case 0x01: // Fill mode
				{
					// Wait for a full quartet
					unsigned long start_time = millis();
					while (Serial.available() < 4) {
						if (millis() - start_time > SLAVE_LOOP_TIMEOUT_MS) {
							do_loop = false;
							break;
						}
					}
					uint8_t red = Serial.read();
					uint8_t green = Serial.read();
					uint8_t blue = Serial.read();
					uint8_t white = Serial.read();
					Display.clear(red, green, blue, white);
					Display.show(0);
				}
					break;
				case 0xFF: // Quit
					do_loop = false;
					break;
				default:
					Serial.write('?');
					Serial.flush();
					break;
			}
		}

		// Continue to fault-handle in this state
		Faults.update();

		// Kick out of the loop if we have thermal problems
		if (Faults.checkFault(FAULT_TSENSE_HARD_LIMIT)) {
			do_loop = false;
		}
	}
}

void status_cb(void) {
    bool new_status = Serial.isConnected();
    if (new_status && !Debug.serial_connected) Debug.reportConnect();
    Debug.serial_connected = new_status;

    if (Debug.serial_connected) Debug.reportStatus();
}