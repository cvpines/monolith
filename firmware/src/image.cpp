#include "image.h"
#include "display.h"
#include "faults.h"
#include "puff.h" // Deflate


const size_t getBufferSize(const uint8_t* image) {
	uint8_t width = image[0];
	uint8_t height = image[1];
	//return 4 * width * height;
	return Display.CHANNELS * width * height;
}

