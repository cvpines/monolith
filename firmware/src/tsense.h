//
// Created by Coriander V. Pines on 2017/03/19.
//

#ifndef TSENSE_H
#define TSENSE_H

#include "application.h"

class TSenseClass {
public:
	static const int TSENSE_INTERVAL_MS = 1000;
    static const int TSENSE_BRD_CURVE_MIN_TENTH_C = 300; // 300 * 0.1 C = 30 C
    static const int TSENSE_LED_CURVE_MIN_TENTH_C = 300; // 300 * 0.1 C = 35 C
	static const int TSENSE_BRD_HARD_MAX_TENTH_C  = 400; // 400 * 0.1 C = 50 C
	static const int TSENSE_LED_HARD_MAX_TENTH_C  = 450; // 450 * 0.1 C = 50 C
	static const int TSENSE_INVALID_MIN_TENTH_C   = 000; // 000 * 0.1 C = 0 C

    int brd_tenth_c = 0;
    int led_tenth_c = 0;
    int brightness_limit = 255;

    TSenseClass(void);
    void init(void);
	void update(void);

private:
    Timer update_timer;
    friend void tsense_cb(void);
};

void tsense_cb(void);

extern TSenseClass TSense;

#endif //TSENSE_H