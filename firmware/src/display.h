//
// Created by Coriander V. Pines on 2017/03/19.
//

#ifndef DISPLAY_H
#define DISPLAY_H

#include "modules.h"
#include "application.h"
#include "neopixel.h"
#include "image.h"
#include "video.h"
#include "ui_images.h"


class DisplayClass {
public:
    static const int WIDTH = 8;
	static const int HEIGHT = 16;
	static const unsigned int CHANNELS = 4;
	static const int BRIGHTNESS_NUMERATOR = 1;
	static const int BRIGHTNESS_DENOMINATOR = 1;
	static const int GLOBAL_BRIGHTNESS_MIN = 48;
	static const int GLOBAL_BRIGHTNESS_MAX = 255;
	static const unsigned long SLEEP_HOLD_DURATION_MS = 1500;
	static const unsigned long PARAM_SET_DURATION_MS = 1000;
	static const unsigned long PARAM_SET_BAR_STEP = 100;
	static const uint32_t PARAM_SET_BAR_COLOR = 0x00001F00;

	unsigned int program_idx = 0;
    bool param_set = false;
	unsigned long param_start_time = 0;
	int brightness = 128;

    DisplayClass();

    void init(void);
    void update(void);

    void enterSleepMode(void);
	void clear(void);
	void clear(uint8_t r, uint8_t g, uint8_t b, uint8_t w);
	void clear(uint32_t color);
	void setPixel(unsigned int x, unsigned y, uint8_t r, uint8_t g, uint8_t b, uint8_t w);
	void setPixel(unsigned int x, unsigned y, uint32_t color);
	int blitImage(const uint8_t* image, int x = 0, int y = 0);


private:
	static const uint8_t DITHER_BITS = 1; // of 8; Rate approx 200Hz / 2^bits

	Adafruit_NeoPixel matrix;
    int brightness_error;
    bool brightness_dither = false;
    uint8_t dither_counter = 0;
    uint8_t buffer[WIDTH * HEIGHT * CHANNELS];

    void updateBrightness(void);
	inline uint8_t adjustColor(uint8_t color, bool dither_offset = false);
    int blitMonochrome(size_t data_size, const uint8_t* data, uint8_t width, uint8_t height, int x=0, int y=0);
	int blitPalette(size_t data_size, const uint8_t* data, uint8_t width, uint8_t height, int x=0, int y=0);
    int blitFullColor(size_t data_size, const uint8_t* data, uint8_t width, uint8_t height, int x=0, int y=0);
	int directBlitRawImage(const uint8_t* image, int x, int y);
	void overlay(unsigned long time);
	void show(unsigned long time);

	#if DEBUG_EN
	friend class DebugClass;
	friend class FaultHandler;
	#endif
};

extern DisplayClass Display;

#endif //DISPLAY_H
