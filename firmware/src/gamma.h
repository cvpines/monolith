//
// Created by Coriander V. Pines on 2017/05/24.
//

#ifndef GAMMA_H
#define GAMMA_H

#include "stdint.h"

extern const uint16_t gamma_10_to_9[];

#endif //GAMMA_H
