#ifndef IMAGE_H
#define IMAGE_H

#include "application.h"
#include <stdint.h>
#include <initializer_list>

//#include "display.h"

enum ImageCompressionMethod {
	IMAGE_UNCOMPRESSED_FULL = 0x00,
	IMAGE_DEFLATE_FULL = 0x01,
	IMAGE_UNCOMPRESSED_PALETTE = 0x02,
	IMAGE_DEFLATE_PALETTE = 0x03,
	IMAGE_UNCOMPRESSED_MONOCHROME = 0x04,
	IMAGE_DEFLATE_MONOCHROME = 0x05,
	//IMAGE_RLE = 0x06,
};

/* Image data structure
	WIDTH
	HEIGHT
	CHANNELS
	COMPRESSION
	RESERVED
	RESERVED
	SIZE_HIGH
	SIZE_LOW
	DATA
	...
*/

const uint32_t IMAGE_COLOR_MASK = 0xFFFF00FF;
const uint8_t IMAGE_MONOCHROME_MASK = 0xFF;


const size_t getBufferSize(const uint8_t * image);


#endif