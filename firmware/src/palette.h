//
// Created by Corey Hoard on 2017/05/24.
//

#ifndef PALETTE_H
#define PALETTE_H

#include <stdint.h>

typedef uint8_t PaletteUniform16_t[64]; // 16 colors * 4 channels interleaved, ~4 bits of blending

// Index a 16-color palette using linear interpolation
uint32_t indexPaletteUniform16(const PaletteUniform16_t palette, uint8_t value);

// Index a 16-color palette using linear interpolation, wrapping around to the first value past 240
uint32_t indexPaletteUniform16Wrap(const PaletteUniform16_t palette, uint8_t value);

extern const PaletteUniform16_t PaletteCandleRGB; // no wrap
extern const PaletteUniform16_t PaletteCandleRGBW; // no wrap
extern const PaletteUniform16_t PaletteCloud; // wrap
extern const PaletteUniform16_t PaletteOcean; // wrap
extern const PaletteUniform16_t PaletteLava; // wrap
extern const PaletteUniform16_t PaletteForest; // wrap
extern const PaletteUniform16_t PaletteBlackbody; // no wrap
extern const PaletteUniform16_t PaletteParty; // wrap

#endif //PALETTE_H
