#ifndef ENCODER_H
#define ENCODER_H

class EncoderClass {
public:
	static const bool ENC_S_INVERT = true; // Child v1.0.0 is active LOW

    volatile int position = 0;
    bool button_state;
    unsigned long button_time;

    void init(void);
    void update(void);

    bool button_pressed(bool clear_flag=true);
    unsigned long button_held(void);
    unsigned long button_released(bool clear_flag=true);
    bool position_changed(void);
	void reset_position(void);

private:
    volatile bool val_a, val_b;
    volatile bool pos_changed_flag = false;
    bool button_pressed_flag = false;
    bool button_released_flag = false;
    friend void enc_a_isr(void);
    friend void enc_b_isr(void);
};

void enc_a_isr(void);
void enc_b_isr(void);

extern EncoderClass Encoder;

#endif //ENCODER_H