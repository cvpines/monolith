//
// Created by Coriander V. Pines on 2017/05/24.
//

#ifndef UI_IMAGES_H
#define UI_IMAGES_H

#include <stdint.h>
#include "image.h"

extern const uint8_t ui_thermal_over_data[];
extern const uint8_t ui_thermal_invalid_data[];

#endif //UI_IMAGES_H
