#ifndef VIDEO_H
#define VIDEO_H

#include "image.h"

/*	Video data structure
	DELAY_HI, DELAY_LO
	RESERVED
	RESERVED
	FRAME_COUNT_HI, FRAME_COUNT_LO
	TABLE_ENTRY_HI, TABLE_ENTRY_M1, TABLE_ENTRY_M2, TABLE_ENTRY_LO
	...
	IMAGE_DATA
	...
*/


enum VideoDisposeMethod {
	VIDEO_REDRAW,
};

enum VideoCompressionMethod {
	VIDEO_IMAGE_SEQUENCE,
};

#endif