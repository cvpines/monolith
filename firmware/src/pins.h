#ifndef PINS_H
#define PINS_H

#include "application.h"
#include "preprocessor.h"

// List of all pin mappings and modes
#define PIN_MAP \
	(PIN_5V_0,			D0, OUTPUT), \
	(PIN_5V_1, 			D1, OUTPUT), \
	(PIN_5V_2, 			D2, OUTPUT), \
	(PIN_5V_3, 			D3, OUTPUT), \
	(PIN_MATRIX,  PIN_5V_0, OUTPUT), \
	(PIN_TSENSE_BRD, 	A0, AN_INPUT), \
	(PIN_TSENSE_LED,    A1, AN_INPUT), \
	(PIN_ISENSE,        A2, AN_INPUT), \
	(PIN_ACCEL_Z,       A3, AN_INPUT), \
	(PIN_ACCEL_Y,       A4, AN_INPUT), \
	(PIN_ACCEL_X,       A5, AN_INPUT), \
	(PIN_LUX,           A6, AN_INPUT), \
	(PIN_AUX,           D7, OUTPUT), \
	(PIN_ENC_S,         D5, INPUT), \
	(PIN_ENC_B,         D6, INPUT), \
	(PIN_ENC_A,         D4, INPUT)
// PIN_AUX:D7 for main board 2.0.0+ or 1.1.0 with patch, D4 without patch
// PIN_ENC_A:D4 for 2.0.0+ or 1.0.0 with patch, D7 without patch


// Expands to enum values for each pin
#define PIN_MAP_ENUM_VALUES \
	PP_FOR_EACH(_PIN_MAP_ENUM_VALUES, null, PIN_MAP)

#define _PIN_MAP_ENUM_VALUES(null, tuple) \
	_PIN_MAP_ENUM_VALUES_(PP_ARGS(tuple))

// Intermediate macro used to expand PP_ARGS(tuple)
#define _PIN_MAP_ENUM_VALUES_(...) \
	_PIN_MAP_ENUM_VALUES__(__VA_ARGS__)

#define _PIN_MAP_ENUM_VALUES__(name, pin, mode) \
	name = pin,

// Expands to pinMode calls for each pin in the mapping
#define PIN_MAP_SET_MODES() \
	do { PP_FOR_EACH(_PIN_MAP_SET_MODES, null, PIN_MAP) } while(0);

#define _PIN_MAP_SET_MODES(null, tuple) \
	_PIN_MAP_SET_MODES_(PP_ARGS(tuple))

// Intermediate macro used to expand PP_ARGS(tuple)
#define _PIN_MAP_SET_MODES_(...) \
	_PIN_MAP_SET_MODES__(__VA_ARGS__)

#define _PIN_MAP_SET_MODES__(name, pin, mode) \
	pinMode(name, mode);


typedef enum pin_mapping {
	// PIN_5V_0 = D0,
	// PIN_5V_1 = D1,
	// ...
	PIN_MAP_ENUM_VALUES
} pin_mapping;


void setup_pins(void);

#endif //PINS_H
