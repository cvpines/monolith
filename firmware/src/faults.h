//
// Created by Coriander V. Pines on 2017/03/19.
//

#ifndef FAULTS_H
#define FAULTS_H

#include "application.h"


enum FaultFlag {
	// Flags
	FAULT_TSENSE_BRD_SOFT_LIMIT = 0x0001,
	FAULT_TSENSE_LED_SOFT_LIMIT = 0x0002,
	FAULT_TSENSE_BRD_HARD_LIMIT = 0x0004,
	FAULT_TSENSE_LED_HARD_LIMIT = 0x0008,
	FAULT_TSENSE_BRD_INVALID 	= 0x0010,
	FAULT_TSENSE_LED_INVALID 	= 0x0020,

	FAULT_IMAGE_INVALID			= 0x0040,
	FAULT_IMAGE_DECOMPRESS		= 0x0080,

	FAULT_VIDEO_INVALID			= 0x0100,
	FAULT_VIDEO_DECOMPRESS		= 0x0200,

	FAULT_WATCHDOG_TIMEOUT		= 0x0300,

	// Fault groups
	FAULT_TSENSE_HARD_LIMIT		= FAULT_TSENSE_BRD_HARD_LIMIT | FAULT_TSENSE_LED_HARD_LIMIT,
	FAULT_TSENSE_INVALID		= FAULT_TSENSE_BRD_INVALID | FAULT_TSENSE_LED_INVALID,
};

class FaultHandler {
public:
	static const int WATCHDOG_TIMEOUT_MS = 5000; // 5s

	FaultHandler(void);

	void init(void);
	void update(void);

	void setFault(uint32_t fault);
	void clearFault(uint32_t fault);
	bool checkFault(uint32_t fault);
	uint32_t getFaults(void);
	void clearDisplay(void);


private:
	uint32_t faults;
	ApplicationWatchdog watchdog;

	friend void watchdog_cb(void);
	friend class DebugClass;
};

extern FaultHandler Faults;

void watchdog_cb(void);

#endif //FAULTS_H
