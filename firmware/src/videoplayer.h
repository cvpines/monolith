#ifndef VIDEO_PLAYER_H
#define VIDEO_PLAYER_H

#include "video.h"



class VideoPlayer {
public:
	VideoPlayer(const uint8_t* data);

	void reset(void);
	bool update(unsigned long time);
	void setFrame(uint16_t frame);
	const uint8_t* getCurrentFrame(void);

private:
	uint16_t delay_ms;
	uint16_t frame_count;
	const uint8_t* frame_offset_table;
	const uint8_t* frame_data;

	unsigned int current_frame;
	unsigned long last_frame_time;
	long timing_error;
};

#endif