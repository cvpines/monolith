#ifndef DEBUG_H
#define DEBUG_H

#include "application.h"
#include "preprocessor.h"

#define SHOW_VAL(EX) do { Serial.print(#EX "="); Serial.println(EX); } while(0)
#define SHOW_GROUP(GROUP, EX) do { Serial.print(#GROUP "/" #EX "="); Serial.println(EX); } while(0)
#define PROFILE(name, func) \
	do { unsigned long PP_CAT(_prof_s_, name) = micros(); \
	func; \
	unsigned long PP_CAT(_prof_e_, name) = micros(); \
	Serial.printf("PROFILE\ndebug/profile_" #name "=%i\n---\n", \
		PP_CAT(_prof_e_, name) - PP_CAT(_prof_s_, name)); } while(0);


class DebugClass {
public:
	static const int STATUS_INTERVAL_MS = 1000; // nominal 1000ms
	static const int SLAVE_LOOP_TIMEOUT_MS = 1000; // should be short as this impacts thermal fault protection

    DebugClass(void);

    bool serial_connected;

    void init(void);
    void update(void);
    void processSerial(void);

    void reportConnect(void);
    void reportStatus(void);

private:
    bool advanced_cmd_armed = false;

    Timer status_timer;
    friend void status_cb(void);

	void slaveLoop(void);
};

void status_cb(void);

extern DebugClass Debug;

#endif // DEBUG_H