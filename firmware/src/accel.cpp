//
// Created by Coriander V. Pines on 2017/03/19.
//

#include "accel.h"

#include "pins.h"

AccelClass Accel;

AccelClass::AccelClass(void) : accel_timer(AccelClass::ACCEL_INTERVAL_MS, accel_cb) {

}

void AccelClass::init(void) {
    accel_timer.start();
}

void accel_cb(void) {
    int x = 0;
    int y = 0;
    int z = 0;

    // Accel is pretty noisy, so average over N samples
    for (int i = 0; i < Accel.ACCEL_MULTISAMPLE; i++) {
        x += analogRead(PIN_ACCEL_X) - 2048;
        y += analogRead(PIN_ACCEL_Y) - 2048;
        z += analogRead(PIN_ACCEL_Z) - 2048;
    }
    x /= Accel.ACCEL_MULTISAMPLE;
    y /= Accel.ACCEL_MULTISAMPLE;
    z /= Accel.ACCEL_MULTISAMPLE;

    Accel.x = x;
    Accel.y = y;
    Accel.z = z;

    int a_x = (x > 0) ? x : -x;
    int a_y = (y > 0) ? y : -y;
    int a_z = (z > 0) ? z : -z;

    if (a_x > a_y) {
        if (a_x > a_z) {
            Accel.up_axis = (Accel.x > 0) ? 1 : -1;
            Accel.u = y;
            Accel.v = z;
            return;
        } else {
            Accel.up_axis = (Accel.z > 0) ? 3 : -3;
            Accel.u = x;
            Accel.v = y;
            return;
        }
    } else {
        if (a_y > a_z) {
            Accel.up_axis = (Accel.y > 0) ? 2 : -2;
            Accel.u = x;
            Accel.v = z;
            return;
        } else {
            Accel.up_axis = (Accel.z > 0) ? 3 : -3;
            Accel.u = x;
            Accel.v = y;
            return;
        }
    }
}
