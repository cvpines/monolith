#include "application.h"
#include "videoplayer.h"

VideoPlayer::VideoPlayer(const uint8_t* data) {
	// Unpack header data
	size_t idx = 0;
	delay_ms = (data[idx++] << 8) | data[idx++];
	idx++; idx++; // Reserved
	frame_count = (data[idx++] << 8) | data[idx++];
	frame_offset_table = &data[idx];
	idx += 3 * frame_count;
	frame_data = &data[idx];

	reset();
}

void VideoPlayer::reset(void) {
	// Set current frame to last so that the next update pushes to first frame
	current_frame = frame_count - 1;
	last_frame_time = 0;
	timing_error = 0;
}

bool VideoPlayer::update(unsigned long time) {
	// Making a decision here to ignore timing errors. Smooth playback in this case is
	// more important than accurate playback rate
	if (time - last_frame_time > delay_ms) {
		timing_error = time - last_frame_time - delay_ms;
		last_frame_time = time;
		current_frame = (current_frame + 1) % frame_count;
		return true;
	} else return false;
}

void VideoPlayer::setFrame(uint16_t frame) {
	if (frame > frame_count) return;
	current_frame = frame;
}

const uint8_t* VideoPlayer::getCurrentFrame(void) {
	// Look up frame offset from table
	size_t frame_offset = 0;
	for (int i = 0; i < 3; i++) {
		frame_offset = (frame_offset << 8) | frame_offset_table[3 * current_frame + i];
	}

	// Return pointer to frame data
	return &frame_data[frame_offset];
}