import logging
import math
import sys
import zlib

from PIL import Image, ImageStat
import scipy.stats


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)



# Types of images this can handle
# * Alpha-encoded monochrome
# * RGB monochrome w/o cast
# * RGB monochrome w/ cast
# * Full RGBW
# * Full RGB
# * Greyscale

# Implemented compression methods:
# * IMAGE_UNCOMPRESSED_MONOCHROME
# * IMAGE_DEFLATE_MONOCHROME
# * IMAGE_UNCOMPRESSED_FULL
# * IMAGE_DEFLATE_FULL

# Known failures:
# * sample_images/greyscale_small_mask.png

# TODO: Palette finding can be done pretty easily using https://en.wikipedia.org/wiki/Vector_quantization 

# TODO: Can also try on small blocks using https://en.wikipedia.org/wiki/Block_Truncation_Coding

# TODO: Can also try a DCT-ish system for specifying color-soups (period, phase, amplitude, mean)

# TODO: Override cast on monochrome images


def formatData(data, width=79, line_start=None):
    def maybeHex(x):
        try:
            return '0x{:02X}'.format(x)
        except ValueError:
            return str(x)

    if line_start is None:
        line_start = '    '

    result = '{\n'
    line = line_start
    for datum in data:
        str_datum = maybeHex(datum)
        if len(line) + len(str_datum) >= width:
            result += line + '\n'
            line = line_start
        line += str_datum + ', '
    result += line + '\n'
        #subset, data = data[:width], data[width:]
        #result += '\t' + ''.join(maybeHex(datum) + ', ' for datum in subset) + '\n'
    result += '}'
    return result


def convertVideo(images, delay, allow_deflate):
    assert delay <= 0xFFFF
    delay_hi = (delay >> 8) & 0xFF
    delay_lo = delay & 0xFF

    assert len(images) <= 0xFFFF
    images_hi = (len(images) >> 8) & 0xFF
    images_lo = len(images) & 0xFF

    header = [delay_hi, delay_lo, 0x00, 0x00, images_hi, images_lo]

    offset_table = []
    frame_table = []
    offset = 0
    logger.info('Compressing %i frames...', len(images))
    for image in images:
        assert offset <= 0xFFFFFF
        offset_table.extend((offset >> (i * 8)) & 0xFF for i in range(2, -1, -1))
        frame_data = convertImage(image, allow_deflate)
        frame_table.extend(frame_data)
        offset += len(frame_data)

    result = header + offset_table + frame_table

    logger.info('Header is %i bytes.', len(header))
    logger.info('Offset table is %i bytes.', len(offset_table))
    logger.info('Frame table is %i bytes.', len(frame_table))
    logger.info('Total video size is %i bytes.', len(result))

    return result


def convertPaletteVideo(palette, images, delay, allow_deflate):
    assert len(palette) == 16*4
    assert delay <= 0xFFFF
    delay_hi = (delay >> 8) & 0xFF
    delay_lo = delay & 0xFF

    assert len(images) <= 0xFFFF
    images_hi = (len(images) >> 8) & 0xFF
    images_lo = len(images) & 0xFF

    header = [delay_hi, delay_lo, 0x00, 0x00, images_hi, images_lo]

    offset_table = []
    frame_table = []
    offset = 0
    logger.info('Compressing %i frames...', len(images))
    for image in images:
        assert offset <= 0xFFFFFF
        offset_table.extend((offset >> (i * 8)) & 0xFF for i in range(2, -1, -1))
        frame_data = convertPaletteImage(palette, image, allow_deflate)
        frame_table.extend(frame_data)
        offset += len(frame_data)

    result = header + offset_table + frame_table

    logger.info('Header is %i bytes.', len(header))
    logger.info('Offset table is %i bytes.', len(offset_table))
    logger.info('Frame table is %i bytes.', len(frame_table))
    logger.info('Total video size is %i bytes.', len(result))

    return result



def convertImage(image, allow_deflate=True, interpolate=False):
    compression_method, data = smartCompressImage(image, allow_deflate, interpolate)

    assert len(data) <= 0xFFFF
    size_hi = (len(data) >> 8) & 0xFF
    size_lo = len(data) & 0xFF

    header = [image.size[0], image.size[1], compression_method, 0x00, 0x00, size_hi, size_lo]

    return header + data

def convertPaletteImage(palette, image, allow_deflate=True):
    compression_method, data = compressPaletteImage(palette, image, allow_deflate)

    assert len(data) <= 0xFFFF
    size_hi = (len(data) >> 8) & 0xFF
    size_lo = len(data) & 0xFF

    header = [image.size[0], image.size[1], compression_method, 0x00, 0x00, size_hi, size_lo]

    return header + data


def smartCompressImage(image, allow_deflate=True, interpolate=False):
    # Convert unknown modes to RGB
    if image.mode not in ('RGBA', 'RGB', 'L'):
        logger.warning('Image mode \'%s\' not supported. Converting to RGB.', image.mode)
        image = image.convert('RGB')

    # Apply image heuristics to optimize compression
    if image.mode == 'RGBA':
        logger.info('Processing RGBA image.')
        stats = ImageStat.Stat(image)

        if all(upper == 0 for lower, upper in stats.extrema[0:3]):
            # Image is alpha-encoded monochrome: (0, 0, 0, w) -> (0, 0, 0, 255) + (w)
            logger.info('Alpha-encoded monochrome detected.')
            mono_image = image.split()[3]
            method, data = compressMonochromeImage(mono_image, (0, 0, 0, 255), allow_deflate)
        elif stats.extrema[3][0] == 255:
            # Image is RGB with unused alpha channel: (r, g, b, 255)
            logger.info('RGB with unused alpha channel detected.')
            # Detect RGB monochrome by removing color cast
            result = decastMonochrome(image)
            if result is None:
                # Full RGB image: (r, g, b) -> (r, g, b, 0)
                # Optionally performs RGB -> RGBW interpolation
                rgbw_image = convertRGBtoRGBW(image, interpolate)
                method, data = compressColorImage(rgbw_image, allow_deflate)
            else:
                # One of:
                # * RGB monochrome w/o cast: (w, w, w) -> (0, 0, 0, 255) + (w)
                # * RGB monochrome w/ cast: (xr, xg, xb) -> (r, g, b, 0) + (x)
                mono_image, color = result
                method, data = compressMonochromeImage(mono_image, color, allow_deflate)
        else:
            # Image is full RGBW: (r, g, b, w) -> (r, g, b, w)
            logger.info('Full RGBW image detected.')
            method, data = compressColorImage(image, allow_deflate)
    elif image.mode == 'L':
        # Image is monochrome W, ready to go
        logger.info('Monochrome W-channel image detected.')
        method, data = compressMonochromeImage(image, (0, 0, 0, 255), allow_deflate)
    elif image.mode == 'RGB':
        logger.info('RGB image detected.')
        # Detect RGB monochrome by removing color cast
        result = decastMonochrome(image)
        if result is None:
            # Full RGB image: (r, g, b) -> (r, g, b, 0)
            # Optionally performs RGB -> RGBW interpolation
            rgbw_image = convertRGBtoRGBW(image, interpolate)
            method, data = compressColorImage(rgbw_image, allow_deflate)
        else:
            # One of:
            # * RGB monochrome w/o cast: (w, w, w) -> (0, 0, 0, 255) + (w)
            # * RGB monochrome w/ cast: (xr, xg, xb) -> (r, g, b, 0) + (x)
            mono_image, color = result
            method, data = compressMonochromeImage(mono_image, color, allow_deflate)
    else:
        raise RuntimeError('Could not convert to known type.')

    return method, data


def compressPaletteImage(palette, image, allow_deflate=True):
    if image.mode != 'L':
        raise RuntimeError('Palette image compression requires mode \'L\'.')
    if len(palette) != 16 and len(palette) != 16*4:
        raise RuntimeError('Palette image compression requires a 16-element RGBW color array')
    if len(palette) == 16:
        flat_palette = sum(palette, [])
    else:
        flat_palette = palette
    assert len(flat_palette) == 16*4

    logger.info('Compressing palette image...')

    # Hold results
    results = dict()   

    # First add the raw data
    logger.info('Trying uncompressed...')
    raw_data = list(flat_palette) + list(image.getdata())
    assert len(raw_data) == (16*4 + image.size[0] * image.size[1])
    results['IMAGE_UNCOMPRESSED_PALETTE'] = raw_data
    logger.info('Size was %i.', len(raw_data))

    # If permitted, try deflate compression
    if allow_deflate:
        logger.info('Trying deflate...')
        deflate_data = list(zlib.compress(bytes(raw_data)))[2:]
        results['IMAGE_DEFLATE_PALETTE'] = deflate_data
        logger.info('Size was %i.', len(deflate_data))

    # That's all we support for now!

    # Compare methods
    method, data = min(results.items(), key=lambda x: len(x[1]))    
    logger.info('Best method was %s with size %i.', method, len(data))

    return method, data


def compressMonochromeImage(image, color=None, allow_deflate=True):
    if image.mode != 'L':
        raise RuntimeError('Monochrome image compression requires mode \'L\'.')
    if len(color) != 4:
        raise RuntimeError('Monochrome image compression requires an RGBW color as a 4-tuple.')

    # Default color cast is full W
    if color is None:
        color = (0, 0, 0, 255)

    logger.info('Compressing monochrome image...')

    # Hold results
    results = dict()   

    # First add the raw data
    logger.info('Trying uncompressed...')
    raw_data = list(color[0:4]) + list(image.getdata())
    assert len(raw_data) == (4 + image.size[0] * image.size[1])
    results['IMAGE_UNCOMPRESSED_MONOCHROME'] = raw_data
    logger.info('Size was %i.', len(raw_data))

    # If permitted, try deflate compression
    if allow_deflate:
        logger.info('Trying deflate...')
        deflate_data = list(zlib.compress(bytes(raw_data)))[2:]
        results['IMAGE_DEFLATE_MONOCHROME'] = deflate_data
        logger.info('Size was %i.', len(deflate_data))

    # That's all we support for now!

    # Compare methods
    method, data = min(results.items(), key=lambda x: len(x[1]))    
    logger.info('Best method was %s with size %i.', method, len(data))

    return method, data



def compressColorImage(image, allow_deflate=True, allow_palette=True):
    if image.mode != 'RGBA':
        raise RuntimeError('Color image compression requires mode \'L\'.')

    logger.info('Compressing color image...')

    # Hold results
    results = dict()   

    # First add the raw data
    logger.info('Trying uncompressed...')
    raw_data = list(sum(list(image.getdata()), ()))
    assert len(raw_data) == (4 * image.size[0] * image.size[1])
    results['IMAGE_UNCOMPRESSED_FULL'] = raw_data
    logger.info('Size was %i.', len(raw_data))

    # If permitted, try deflate compression
    if allow_deflate:
        logger.info('Trying deflate...')
        deflate_data = list(zlib.compress(bytes(raw_data)))[2:]
        results['IMAGE_DEFLATE_FULL'] = deflate_data
        logger.info('Size was %i.', len(deflate_data))

    # TODO: Need to make a custom solution for paletting
    # image.colors(image.size[0] * image.size[1]) is helpful

    # That's all we support for now!

    # Compare methods
    method, data = min(results.items(), key=lambda x: len(x[1]))    
    logger.info('Best method was %s with size %i.', method, len(data))

    return method, data


def convertRGBtoRGBW(image, interpolate=False):
    logger.info('Converting RGB to RGBW.')
    
    if interpolate:
        # TODO: Convert RGB to RGBW using http://blog.saikoled.com/post/44677718712/how-to-convert-from-hsi-to-rgb-white or something
        raise NotImplementedError
    else:
        image.putalpha(0)
        return image


# Analyze an RGB image to determine if it is color-cast monochrome
def decastMonochrome(image, correlation_thresh=0.95, white_thresh=0.99, rescale_values=False):
    # TODO: Detect mask color and translate appropriately
    # Ideas: remove mask pixels from analysis, then map 255 to mask afterwards

    if image.mode != 'RGB':
        raise RuntimeError('decastMonochrome requires RGB image.')

    logger.info('Attempting to decast monochrome image...')

    # Compute general stats (uses mean and extrema)
    stats = ImageStat.Stat(image)

    # Perform regression against highest mean
    sorted_bands = sorted([0, 1, 2], key=lambda x: stats.mean[x]) # Band-indices sorted ascending by mean brightness
    logger.info('Calculating correlation using \'%s\' band.', 'RGB'[sorted_bands[-1]])
    min_band, med_band, max_band = (list(image.getdata(band)) for band in sorted_bands)
    try:
        max_min_regress = scipy.stats.linregress(max_band, min_band)
        max_med_regress = scipy.stats.linregress(max_band, med_band)
    except Exception:
        logger.info('Regression failed.')
        return None

    # Check that the correlation is sufficient
    if (max_min_regress.rvalue < correlation_thresh) or (max_med_regress.rvalue < correlation_thresh):
        logger.info('Correlation insufficient: (%f, %f) < %f', max_min_regress.rvalue, max_med_regress.rvalue, correlation_thresh)
        return None
    else:
        logger.info('Correlation above threshold: (%f, %f) >= %f', max_min_regress.rvalue, max_med_regress.rvalue, correlation_thresh)

    # Build normalized color cast from regression slopes (max = 1.0)
    norm_cast = [0.0, 0.0, 0.0]
    norm_cast[sorted_bands[0]] = max_min_regress.slope
    norm_cast[sorted_bands[1]] = max_med_regress.slope
    norm_cast[sorted_bands[2]] = 1.0

    if rescale_values:
        # Find a value that makes the greatest brightness to encode be 255. This preserves maxiumum resolution
        # This value will never be greater than 1.0
        rescaler = min(bound[1] / cast_band for bound, cast_band in zip(stats.extrema, norm_cast)) / 255.0
    else:
        rescaler = 1.0
    rescaled_cast = [cast_band * rescaler for cast_band in norm_cast]
    logger.info('Using rescale value %f.', rescaler)

    # Detect white-only images as a special case
    if (max_min_regress.slope >= white_thresh) and (max_med_regress.slope >= white_thresh):
        monochrome_color = [0, 0, 0, min(255, max(0, int(255 * rescaler)))]
        logger.info('Using only W channel to encode, as (%f, %f) >= %f', max_min_regress.slope, max_med_regress.slope, white_thresh)
    else:
        monochrome_color = [min(255, max(0, int(255 * cast_band))) for cast_band in rescaled_cast]
        monochrome_color.append(0) # Add in unused W channel
        logger.info('Detected cast color: %s', str(monochrome_color))

    # Use the brightest band to decast
    encode_band = sorted_bands[-1]
    greyscale_image_data = [min(255, max(0, int(pixel / rescaled_cast[encode_band]))) for pixel in image.getdata(encode_band)]

    # Return the cast color and recovered greyscale image
    return Image.frombytes('L', image.size, bytes(greyscale_image_data)), monochrome_color
