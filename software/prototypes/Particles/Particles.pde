
int M_WIDTH = 8;
int M_HEIGHT = 16;
int M_CHANNELS = 4;
int M_OVERFLOW = 32;

int[] M_DISPLAY;
int[] M_BUFFER;

int[] RANDOM;
int[] GAUSS;
int FRAME = 0;


class Particle {
  float px, py, vx, vy;

  Particle(float x, float y) {
    px = x;
    py = y;
    vx = random(-2, 2);
    vy = random(-2, 2);
  }

  void update(float dt) {
    px += vx * dt;
    py += vy * dt;
    
 
    
    if (vx*vx*vy*vy < 0.001) {
      //px = random(0, M_WIDTH);
      //py = random(0, M_HEIGHT);
    }
  }

  void springTo(float x, float y, float k) {
    vx +=  (x - px) * k;
    vy +=  (y - py) * k;
  }

  void gravTo(float x, float y, float k) {
    float dx = (x - px);
    float dy = (y - py);
    float r3 = pow(dx*dx+dy*dy, 1.5) + 0.1;
    vx +=  dx * k / r3;
    vy +=  dy * k / r3;
  }

  void drag(float d) {
    vx += -d * vx;
    vy += -d * vy;
  }
};

Particle[] Particles;

void setup() {
  size(400, 800);

  M_DISPLAY = new int[M_WIDTH * M_HEIGHT * M_CHANNELS + M_OVERFLOW];
  M_BUFFER = new int[M_WIDTH * M_HEIGHT * M_CHANNELS + M_OVERFLOW];

  RANDOM = new int[256];
  for (int i = 0; i < RANDOM.length; i++) {
    RANDOM[i] = (int)random(256);
  }

  GAUSS = new int[8];
  for (int i = 0; i < GAUSS.length; i++) {
    float x = 1.5 * (float)i / (GAUSS.length - 1);
    GAUSS[i] = int(255 * exp(-x*x));
  }


  Particles = new Particle[16];
  for (int i = 0; i < Particles.length; i++) {
    Particles[i] = new Particle(random(0, M_WIDTH), random(0, M_HEIGHT));
  }

  clear_buffer(M_BUFFER);
  copy_buffer(M_BUFFER, M_DISPLAY);

  frameRate(15);
}

void draw() {
  FRAME++;


  clear_buffer(M_DISPLAY, 32);


  for (int p = 0; p < Particles.length; p++) {
    for (int ii = 0; ii < 1; ii++) {
      // Particles[p].springTo(M_WIDTH * mouseX / width, M_HEIGHT * mouseY / height, 1);
      //Particles[p].gravTo(4, 8, -0.5);
      Particles[p].springTo(4, 8, 0.001);
      Particles[p].gravTo(0, 0, 1);
      Particles[p].gravTo(0, 16, 1);
      Particles[p].gravTo(8, 8, 1);
     // Particles[p].gravTo(Particles[(p+1)%3].px, Particles[(p+1)%3].py, -0.9);
     // Particles[p].gravTo(Particles[(p+2)%3].px, Particles[(p+2)%3].py, -0.9);
      //Particles[p].drag(0.005);
      Particles[p].update(0.05);
    }

    int ix = int(Particles[p].px);
    int iy = int(Particles[p].py);

    for (int i = -GAUSS.length + 1; i < GAUSS.length; i++) {
      for (int j = -GAUSS.length + 1; j < GAUSS.length; j++) {
        float gauss = float(GAUSS[abs(i)] * GAUSS[abs(j)]) / (255 * 255);
        add_pixel(M_DISPLAY, ix + j, iy + i, int(127 * (float(p)/Particles.length) * gauss), int(127 *  (1.0 - float(p)/Particles.length) * gauss), int(255 * 0.0 * gauss), int(255 * 0.0 * gauss));
      }
    }
  }

  draw_display();
}


int pixelIndex(int x, int y) {
  return (M_WIDTH * y + x) * M_CHANNELS;
}

int screenIdex(int x, int y) {
  return (M_WIDTH * (M_HEIGHT * y / height) + (M_WIDTH * x) / width) * M_CHANNELS;
}

void set_pixel(int[] buffer, int x, int y, int r, int g, int b, int w) {
  if ((x < 0) || (x >= M_WIDTH) || (y < 0) || (y >= M_HEIGHT)) {
    return;
  }
  int idx = pixelIndex(x, y);
  buffer[idx + 0] = r;
  buffer[idx + 1] = g;
  buffer[idx + 2] = b;
  buffer[idx + 3] = w;
}

void add_pixel(int[] buffer, int x, int y, int r, int g, int b, int w) {
  if ((x < 0) || (x >= M_WIDTH) || (y < 0) || (y >= M_HEIGHT)) {
    return;
  }
  int idx = pixelIndex(x, y);
  buffer[idx + 0] += r;
  buffer[idx + 1] += g;
  buffer[idx + 2] += b;
  buffer[idx + 3] += w;
}


void swap_buffer(int[] a, int[] b) {
  int tmp;
  for (int i = 0; i < a.length; i++) {
    tmp = a[i];
    a[i] = b[i];
    b[i] = tmp;
  }
}

void copy_buffer(int[] src, int[] dst) {
  for (int i = 0; i < dst.length; i++) {
    dst[i] = src[i];
  }
}

void clear_buffer(int[] buffer) {
  clear_buffer(buffer, 0);
}

void clear_buffer(int[] buffer, int w) {
  clear_buffer(buffer, 0, 0, 0, w);
}

void clear_buffer(int[] buffer, int r, int g, int b, int w) {
  for (int i = 0; i < M_WIDTH * M_HEIGHT; i++) {
    buffer[i * M_CHANNELS] = r;
    buffer[i * M_CHANNELS + 1] = g;
    buffer[i * M_CHANNELS + 2] = b;
    buffer[i * M_CHANNELS + 3] = w;
  }
}

void draw_display() {
  float h = (float)height / M_HEIGHT;
  float w = (float)width / M_WIDTH;
  int c_r, c_g, c_b, c_w;
  for (int y = 0; y < M_HEIGHT; y++) {
    for (int x = 0; x < M_WIDTH; x++) {
      c_r = M_DISPLAY[(y * M_WIDTH + x) * M_CHANNELS];
      c_g = M_DISPLAY[(y * M_WIDTH + x) * M_CHANNELS + 1];
      c_b = M_DISPLAY[(y * M_WIDTH + x) * M_CHANNELS + 2];
      c_w = M_DISPLAY[(y * M_WIDTH + x) * M_CHANNELS + 3];
      c_w += max(0, c_r - 255);
      c_w += max(0, c_g - 255);
      c_w += max(0, c_b - 255);
      noStroke();
      fill(color(c_r + c_w, c_g + c_w, c_b + c_w));
      rect(x * w, y * h, w, h);
    }
  }
}