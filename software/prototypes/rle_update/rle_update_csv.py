# Video compression proof of concept

# Data flow
# Split into planes
# For each linear plane, discard unchanged pixels
# Identify runs and lengths

from PIL import Image
import sys


width = 9
height = 16
gamma_correct = False
gamma_value = 2.7

# NOTE: Prototype only processes single plane images
frame_stack = list()

for name in sys.argv[1:]: # For each image passed to script...
	image = Image.open(name)
	if image.mode != 'L': # Not grayscale? Convert it
		image = image.convert("L")
	frame_stack.append(list(image.getdata()))
	image.close()
	
	'''
	# Gamma correction:
	for y in range(image.size[1]):
		for x in range(image.size[0]):
			image.pixels[x, y] = int(pow(
			  (image.pixels[x, y] / 255.0), 2.7) * 255.0 + 0.5
	'''


# First frame is always preserved
diff_stack = [frame_stack[0]]
for frame_num in range(1, len(frame_stack)):
	diff_stack.append([cur if cur != prev else None for cur, prev in zip(frame_stack[frame_num], frame_stack[frame_num - 1])])

'''
diff_stack = [
[None, None, 1, 2, 3, None, None, 2, None, 4, 5, 6],
[2, 3, None, None, None, 2, 3, 4, None]
]
'''

# Output runs
for diff_frame in diff_stack:
	new_pixel_run = False
	run_length = 0
	for index in range(len(diff_frame)):
		pixel = diff_frame[index]
		if new_pixel_run:
			# In the middle of a run of new pixels
			if pixel is None:
				# The run has ended
				print run_length
				print '\n'.join([str(x) for x in diff_frame[index - run_length:index]])
				run_length = 0
				new_pixel_run = False
		else:
			# In the middle of a run of unchanged pixels
			if pixel is not None:
				# The run has ended
				print run_length
				run_length = 0
				new_pixel_run = True
		run_length += 1
	if new_pixel_run:
		print run_length
		print '\n'.join([str(x) for x in diff_frame[-run_length:]])
	else:
		print run_length
	print '-'


import time
for frame in frame_stack:
	for pixel in range(len(frame)):
		if frame[pixel] is None:
			print ' ',
		elif frame[pixel] > 192:
			print '@',
		elif frame[pixel] > 128:
			print 'O',
		elif frame[pixel] > 64:
			print '*',
		else:
			print ' ',
		if pixel % 9 == 8:
			print '---'
	time.sleep(0.1)

