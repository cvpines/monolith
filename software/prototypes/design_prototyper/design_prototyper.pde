int M_WIDTH = 8;
int M_HEIGHT = 16;
int M_CHANNELS = 4;
int M_OVERFLOW = 32;

int[] M_DISPLAY;
int[] M_BUFFER;

int[] RANDOM;
int FRAME = 0;


void setup() {
  size(400, 800);

  M_DISPLAY = new int[M_WIDTH * M_HEIGHT * M_CHANNELS + M_OVERFLOW];
  M_BUFFER = new int[M_WIDTH * M_HEIGHT * M_CHANNELS + M_OVERFLOW];
  
  RANDOM = new int[256];
  for (int i = 0; i < RANDOM.length; i++) {
    RANDOM[i] = (int)random(256);
  }

  clear_buffer(M_BUFFER);
  copy_buffer(M_BUFFER, M_DISPLAY);
  
  frameRate(15);
}

void draw() {
  FRAME++;
  
  
  //swap_buffer(M_DISPLAY, M_BUFFER);
  if (mousePressed) {
  M_DISPLAY[pixelIndex(mouseX, mouseY) + 3] = 255;
  }
  
  
  for (int x = 0; x < M_WIDTH; x++) {
    M_DISPLAY[((M_HEIGHT - 1) * M_WIDTH + x) * M_CHANNELS + 0] = RANDOM[((FRAME * 29) + x) % 256];// > 127 ? 255 : 0;
    M_DISPLAY[((M_HEIGHT - 1) * M_WIDTH + x) * M_CHANNELS + 1] = RANDOM[((FRAME * 29) + x) % 256] / 3;// > 127 ? 255 : 0;
    M_DISPLAY[((M_HEIGHT - 1) * M_WIDTH + x) * M_CHANNELS +3] = RANDOM[((FRAME * 17) + x) % 256] / 2;// > 127 ? 255 : 0;
  }
  //swap_buffer(M_DISPLAY, M_BUFFER);
 
  //copy_buffer(M_BUFFER, M_DISPLAY);
  waterfall();
//  smear();
  draw_display();
}

int pixelIndex(int x, int y) {
  return (M_WIDTH * (M_HEIGHT * y / height) + (M_WIDTH * x) / width) * M_CHANNELS;
}


void waterfall() {
   for (int y = 0; y < M_HEIGHT - 1; y++) {
    for (int x = 0; x < M_WIDTH; x++) {
      for (int c = 0; c < M_CHANNELS; c++) {
        int val = 3 * M_DISPLAY[(y * M_WIDTH + x) * M_CHANNELS + c] / 4;
        M_BUFFER[(y * M_WIDTH + x) * M_CHANNELS + c] = M_BUFFER[((y+1) * M_WIDTH + x) * M_CHANNELS + c];
          
        //if (val > 0) val -= 1;
        //val /= 2;
        M_DISPLAY[(y * M_WIDTH + x) * M_CHANNELS + c] = val + M_BUFFER[(y * M_WIDTH + x) * M_CHANNELS + c];
      }
    }
  }
}

void smear() {
   copy_buffer(M_DISPLAY, M_BUFFER);
   for (int y = 0; y < M_HEIGHT - 1; y++) {
    for (int x = 0; x < M_WIDTH; x++) {
      for (int c = 0; c < M_CHANNELS; c++) {
        int val = M_BUFFER[(y * M_WIDTH + x) * M_CHANNELS + c];
        val += M_BUFFER[((y+1) * M_WIDTH + x) * M_CHANNELS + c];
        val /= 2;
        M_DISPLAY[(y * M_WIDTH + x) * M_CHANNELS + c] = val;
      }
    }
  }
}

void flame() {
  copy_buffer(M_DISPLAY, M_BUFFER);
   for (int y = 0; y < M_HEIGHT - 1; y++) {
    for (int x = 0; x < M_WIDTH; x++) {
      for (int c = 0; c < M_CHANNELS; c++) {
        int val = M_BUFFER[(y * M_WIDTH + x) * M_CHANNELS + c];
        val += M_BUFFER[((y+1) * M_WIDTH + x - 1) * M_CHANNELS + c];
        val += M_BUFFER[((y+1) * M_WIDTH + x) * M_CHANNELS + c];
        val += M_BUFFER[((y+1) * M_WIDTH + x + 1) * M_CHANNELS + c];
        if (val > 0) val -= 1;
        val /= 4;
        M_DISPLAY[(y * M_WIDTH + x) * M_CHANNELS + c] = val;
      }
    }
  }
}

void swap_buffer(int[] a, int[] b) {
  int tmp;
  for (int i = 0; i < a.length; i++) {
    tmp = a[i];
    a[i] = b[i];
    b[i] = tmp;
  }
}

void copy_buffer(int[] src, int[] dst) {
  for (int i = 0; i < dst.length; i++) {
    dst[i] = src[i];
  }
}

void clear_buffer(int[] buffer) {
  clear_buffer(buffer, 0);
}

void clear_buffer(int[] buffer, int w) {
  clear_buffer(buffer, 0, 0, 0, w);
}

void clear_buffer(int[] buffer, int r, int g, int b, int w) {
  for (int i = 0; i < M_WIDTH * M_HEIGHT; i++) {
    buffer[i * M_CHANNELS] = r;
    buffer[i * M_CHANNELS + 1] = g;
    buffer[i * M_CHANNELS + 2] = b;
    buffer[i * M_CHANNELS + 3] = w;
  }
}

void draw_display() {
  float h = (float)height / M_HEIGHT;
  float w = (float)width / M_WIDTH;
  int c_r, c_g, c_b, c_w;
  for (int y = 0; y < M_HEIGHT; y++) {
    for (int x = 0; x < M_WIDTH; x++) {
      c_r = M_DISPLAY[(y * M_WIDTH + x) * M_CHANNELS];
      c_g = M_DISPLAY[(y * M_WIDTH + x) * M_CHANNELS + 1];
      c_b = M_DISPLAY[(y * M_WIDTH + x) * M_CHANNELS + 2];
      c_w = M_DISPLAY[(y * M_WIDTH + x) * M_CHANNELS + 3];
      noStroke();
      fill(color(c_r + c_w, c_g + c_w, c_b + c_w));
      rect(x * w, y * h, w, h);
    }
  }
}