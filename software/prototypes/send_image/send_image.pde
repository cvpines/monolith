import themidibus.*; //Import the library
import processing.serial.*;

MidiBus myBus; // The MidiBus
Serial myPort;  // Create object from Serial class
PImage bg;

int knob[] = new int[8];

void setup() {
  size(400, 800);
  background(0);

  myBus = new MidiBus(this, "MPK mini", -1); // Create a new MidiBus with no input device and the default Java Sound Synthesizer as the output device.

  println(Serial.list());
  String portName = Serial.list()[1];
  myPort = new Serial(this, portName, 9600);

  for (int y = 0; y < 16; y++) {
    for (int i = 0; i < 4*8; i++) myPort.write(0xFF); // Exit slave mode
    delay(1);
  }
  myPort.write("as"); // Enter slave mode
  
  bg = loadImage("moonwalk.jpg");
}

void draw() {
  background(0);


  myPort.write(0x01); // Fill mode
   for (int i = 0; i < 4; i++) {
   int val = constrain(2 * knob[i] + (knob[i+4] / 16 - 3), 0, 255);
   myPort.write(val);
   ellipse(50 + 100 * (i % 4), 200, val, val);
   }
   
  while ( myPort.available() > 0) {  // If data is available,
    print((char)myPort.read());         // read it and store it in val
  }
}

void controllerChange(int channel, int number, int value) {
  // Receive a controllerChange
  knob[number - 1] = value;
}