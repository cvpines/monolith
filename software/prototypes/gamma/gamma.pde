import processing.serial.*;

Serial myPort;  // Create object from Serial class

void setup() {
  size(400, 800);
  background(0);
frameRate(999999);

  println(Serial.list());
  String portName = Serial.list()[1];
  myPort = new Serial(this, portName, 9600);

  for (int y = 0; y < 16; y++) {
    for (int i = 0; i < 4*8; i++) myPort.write(0xFF); // Exit slave mode
    delay(1);
  }
  myPort.write("as"); // Enter slave mode
}

float gamma = 2.8;

boolean phase = false;
void draw() {
  phase = !phase;
  background(0);

  text("Gamma: " + gamma, 15, 105);
  
  // Initialize raw transfer
  myPort.write(0x00); // Raw mode

  // Two lines of space
  for (int y = 0; y < 2; y++) {
    for (int x = 0; x < 8; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
    }
    delay(1);
  }

  // Four lines of white
  for (int y = 2; y < 6; y++) {
    // Two columns of black
    for (int x = 0; x < 2; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
    }
    // Four columns of white
    for (int x = 2; x < 6; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write((byte)(pow(128.0 / 255.0, gamma) * 255));
    }
    // Two columns of black
    for (int x = 6; x < 8; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
    }
    delay(1);
  }
  
    // Four lines of space
  for (int y = 6; y < 10; y++) {
    for (int x = 0; x < 8; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
    }
    delay(1);
  }
  
    // Four lines of color
  for (int y = 10; y < 14; y++) {
    // Two columns of black
    for (int x = 0; x < 2; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
    }
    // Four columns of alternate
    for (int x = 2; x < 6; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write((phase != (0 == ((x + y) & 1))) ? 0xFF : 0x00);
    }
    // Two columns of black
    for (int x = 6; x < 8; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
    }
    delay(1);
  }
  
  // Two lines of space
  for (int y = 14; y < 16; y++) {
    for (int x = 0; x < 8; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
    }
    delay(1);
  }


  /*
  myPort.write(0x00); // Raw mode
   for (int y = 0; y < 16; y++) {
   for (int x = 0; x < 8; x++) {
   int r, g, b, ct;
   r = g = b = ct = 0;
   for (int xc = 0; xc < 50; xc += 2) {
   for (int yc = 0; yc < 50; yc += 2) {
   ct++;
   color c = get(x * 50 + xc, y * 50 + yc);
   r += red(c);
   g += green(c);
   b += blue(c);
   }
   }
   float gamma = 2.8;
   myPort.write((byte)(pow(r / ((float)ct * 255.0), gamma) * 255));
   myPort.write((byte)(pow(g / ((float)ct * 255.0), gamma) * 255));
   myPort.write((byte)(pow(b / ((float)ct * 255.0), gamma) * 255));
   myPort.write(0x00);
   set(x * 50 + 1, y * 50, color(128));
   }
   delay(1);
   }
   */

  // Show incoming data
  while ( myPort.available() > 0) {
    print((char)myPort.read());
  }
}

void keyPressed() {
  if (key == 'q') {
    end();
  } else if (key == 'a') {
    gamma -= 0.1;
  } else if (key == 'A') {
    gamma += 0.1;
  }
}

void end() {
  // Exit slave mode
  for (int y = 0; y < 16; y++) {
    for (int i = 0; i < 4*8; i++) myPort.write(0xFF);
    delay(1);
  }

  // Clear incoming buffer
  while ( myPort.available() > 0) myPort.read();

  exit();
}