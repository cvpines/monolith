import themidibus.*; //Import the library
import processing.serial.*;

MidiBus myBus; // The MidiBus
Serial myPort;  // Create object from Serial class
PImage bg;

int knob[] = new int[8];

void setup() {
  size(400, 800);
  background(0);

  myBus = new MidiBus(this, "MPK mini", -1); // Create a new MidiBus with no input device and the default Java Sound Synthesizer as the output device.

  println(Serial.list());
  String portName = Serial.list()[1];
  myPort = new Serial(this, portName, 9600);

  for (int y = 0; y < 16; y++) {
    for (int i = 0; i < 4*8; i++) myPort.write(0xFF); // Exit slave mode
    delay(1);
  }
  myPort.write("as"); // Enter slave mode
  
  bg = loadImage("moonwalk.jpg");
}

void draw() {
  background(0);



  /*
  myPort.write(0x01); // Fill mode
   for (int i = 0; i < 4; i++) {
   int val = constrain(2 * knob[i] + (knob[i+4] / 16 - 3), 0, 255);
   myPort.write(val);
   ellipse(50 + 100 * (i % 4), 200, val, val);
   }
   */
  noStroke();
  fill(70, 70, 90);
  triangle(mouseX, mouseY, 200, 200, 30, 40);
 // image(bg, mouseX, mouseY);

fill(120, 0, 39);
  pushMatrix();
  translate(mouseX, mouseY);
  rotate(0.002 * millis());
  rect(-25, -100, 60, 300);
  popMatrix();


  myPort.write(0x00); // Raw mode
  for (int y = 0; y < 16; y++) {
    for (int x = 0; x < 8; x++) {
      int r, g, b, ct;
      r = g = b = ct = 0;
      for (int xc = 0; xc < 50; xc += 2) {
        for (int yc = 0; yc < 50; yc += 2) {
          ct++;
          color c = get(x * 50 + xc, y * 50 + yc);
          r += red(c);
          g += green(c);
          b += blue(c);
        }
      }
      float gamma = map(knob[0], 0, 127, 1.0, 2.8);
      myPort.write((byte)(pow(r / ((float)ct * 255.0), gamma) * 255));
      myPort.write((byte)(pow(g / ((float)ct * 255.0), gamma) * 255));
      myPort.write((byte)(pow(b / ((float)ct * 255.0), gamma) * 255));
      myPort.write(0x00);
      set(x * 50 + 1, y * 50, color(128));
    }
    delay(1);
  }

  while ( myPort.available() > 0) {  // If data is available,
    print((char)myPort.read());         // read it and store it in val
  }
}

void controllerChange(int channel, int number, int value) {
  // Receive a controllerChange
  knob[number - 1] = value;
}