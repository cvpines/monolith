import processing.serial.*;

Serial myPort;  // Create object from Serial class

class RGBW {
  RGBW(int r, int g, int b, int w) {red = r; green = g; blue = b; white = w; }
  int red;
  int green;
  int blue;
  int white;
};

RGBW linearExtrapolate(int red, int green, int blue, float alpha) {
  // Factions of max that correspond to 100% W   
  float ratios[] = {1.515, 1.258, 0.852};
  
  // Find white component
  float white = alpha * min(red / ratios[0], green / ratios[1], blue / ratios[2]);
  
  return new RGBW(int(red - white * ratios[0]), 
                  int(green - white * ratios[1]), 
                  int(blue - white * ratios[2]),
                  int(white));
}


void setup() {
  size(400, 800);
  background(0);

  println(Serial.list());
  String portName = Serial.list()[1];
  myPort = new Serial(this, portName, 9600);

  for (int y = 0; y < 16; y++) {
    for (int i = 0; i < 4*8; i++) myPort.write(0xFF); // Exit slave mode
    delay(1);
  }
  myPort.write("as"); // Enter slave mode
}


int brightness = 128;

// 2.8: 128 -> 194, 161, 109
// converts to 0.659 -> 1.0, 0.829, 0.561
// converts to 1.0 -> 1.515, 1.258, 0.852


int red = 128;
int green = 128;
int blue = 128;
float gamma = 2.8;
float alpha = 0.5;


void draw() {
  background(0);

  int red = int(255 * (0.5 + 0.5 * cos(millis() * 0.0005)));
  int green = int(255 * (0.5 + 0.5 * cos(millis() * 0.00073)));
  int blue = int(255 * (0.5 + 0.5 * cos(millis() * 0.00097)));

  RGBW conv = linearExtrapolate(red, green, blue, alpha);


  text("Red: " + conv.red, 15, 15);
  text("Green: " + conv.green, 15, 30);
  text("Blue: " + conv.blue, 15, 45);
  text("White: " + conv.white, 15, 60);
  

  text("Red: " + red, 15, 90);
  text("Green: " + green, 15, 105);
  text("Blue: " + blue, 15, 120);
  
  text("Gamma: " + gamma, 15, 150);
  text("Alpha: " + alpha, 15, 165);
  
  // Initialize raw transfer
  myPort.write(0x00); // Raw mode

  // Two lines of space
  for (int y = 0; y < 2; y++) {
    for (int x = 0; x < 8; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
    }
    delay(1);
  }

  // Four lines of white
  for (int y = 2; y < 6; y++) {
    // Two columns of black
    for (int x = 0; x < 2; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
    }
    // Four columns of white
    for (int x = 2; x < 6; x++) {
      myPort.write((byte)(pow(conv.red / 255.0, gamma) * 255));
      myPort.write((byte)(pow(conv.green / 255.0, gamma) * 255));
      myPort.write((byte)(pow(conv.blue / 255.0, gamma) * 255));
      myPort.write((byte)(pow(conv.white / 255.0, gamma) * 255));
    }
    // Two columns of black
    for (int x = 6; x < 8; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
    }
    delay(1);
  }
  
    // Four lines of space
  for (int y = 6; y < 10; y++) {
    for (int x = 0; x < 8; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
    }
    delay(1);
  }
  
    // Four lines of color
  for (int y = 10; y < 14; y++) {
    // Two columns of black
    for (int x = 0; x < 2; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
    }
    // Four columns of color
    for (int x = 2; x < 6; x++) {
      myPort.write((byte)(pow(red / 255.0, gamma) * 255));
      myPort.write((byte)(pow(green / 255.0, gamma) * 255));
      myPort.write((byte)(pow(blue / 255.0, gamma) * 255));
      myPort.write(0x00);
    }
    // Two columns of black
    for (int x = 6; x < 8; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
    }
    delay(1);
  }
  
  // Two lines of space
  for (int y = 14; y < 16; y++) {
    for (int x = 0; x < 8; x++) {
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
      myPort.write(0x00);
    }
    delay(1);
  }


  /*
  myPort.write(0x00); // Raw mode
   for (int y = 0; y < 16; y++) {
   for (int x = 0; x < 8; x++) {
   int r, g, b, ct;
   r = g = b = ct = 0;
   for (int xc = 0; xc < 50; xc += 2) {
   for (int yc = 0; yc < 50; yc += 2) {
   ct++;
   color c = get(x * 50 + xc, y * 50 + yc);
   r += red(c);
   g += green(c);
   b += blue(c);
   }
   }
   float gamma = 2.8;
   myPort.write((byte)(pow(r / ((float)ct * 255.0), gamma) * 255));
   myPort.write((byte)(pow(g / ((float)ct * 255.0), gamma) * 255));
   myPort.write((byte)(pow(b / ((float)ct * 255.0), gamma) * 255));
   myPort.write(0x00);
   set(x * 50 + 1, y * 50, color(128));
   }
   delay(1);
   }
   */

  // Show incoming data
  while ( myPort.available() > 0) {
    print((char)myPort.read());
  }
}

void keyPressed() {
  if (key == 'q') {
    end();
  } else if (key == 'r') {
    red--;
  } else if (key == 'R') {
    red++;
  } else if (key == 'g') {
    green--;
  } else if (key == 'G') {
    green++;
  } else if (key == 'b') {
    blue--;
  } else if (key == 'B') {
    blue++;
  } else if (key == 'a') {
    gamma -= 0.1;
  } else if (key == 'A') {
    gamma += 0.1;
  } else if (key == 's') {
    alpha -= 0.05;
  } else if (key == 'S') {
    alpha += 0.05;
  }
}

void end() {
  // Exit slave mode
  for (int y = 0; y < 16; y++) {
    for (int i = 0; i < 4*8; i++) myPort.write(0xFF);
    delay(1);
  }

  // Clear incoming buffer
  while ( myPort.available() > 0) myPort.read();

  exit();
}