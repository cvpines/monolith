import asyncio
import time
import re
import json
import serial
import serial.aio
import serial.tools.list_ports
import websockets

# Packets to send to socket
packet_queue = asyncio.Queue()

# Async websocket server
async def serve_packets(websocket, path):
    while True:
        packet = await packet_queue.get()
        print('sending packet')
        await websocket.send(packet)
        packet_queue.task_done()


# Async serial reader
class MonolithSerialPacket(asyncio.Protocol):

    def connection_made(self, transport):
        self.transport = transport
        self.buffer = b''
        transport.serial.rts = False
        print('port opened')

    def data_received(self, data):
        # Buffer an check for any full packets, putting incomplete packets
        # back into the buffer
        self.buffer += data
        packets = self.buffer.split(b'---\n')
        self.buffer = packets[-1]

        # Process each new packet (skip incomplete packets)
        for packet in packets[:-1]:
            #print('got packet')
            self.process_packet(packet.decode("utf-8"))

    def process_packet(self, packet):
        lines = packet.split('\n')
        packet_name = lines[0]

        # Skip first and last line (first is title, last is empty)
        for packet_line in lines[1:-1]:
            try:
                category, var_pairs = packet_line.split('/', 1)
            except:
                print('BAD' + str(packet_line))
            for var_pair in var_pairs.split('\t'):
                # TODO: Check annotators here
                try:
                    var_name, val = var_pair.split('=', 1)
                except:
                    print('BAD' + str(var_pair))
                packet_dict = {category.lower() + '/' + var_name.lower(): {'val': val, 'packet': packet_name, 'time': time.time()}}
                packet_queue.put_nowait(json.dumps(packet_dict))

    def connection_lost(self, exc):
        print('port closed')
        self.transport.close()
        asyncio.get_event_loop().stop()

def findDevice():
    for port in serial.tools.list_ports.comports():
        if port.description == 'Photon':
            return port.device
    return None

loop = asyncio.get_event_loop()
serial_coro = serial.aio.create_serial_connection(loop, MonolithSerialPacket, findDevice(), baudrate=9600)
loop.run_until_complete(serial_coro)
server_coro = websockets.serve(serve_packets, '127.0.0.1', 5678)
loop.run_until_complete(server_coro)
print('Server running at 127.0.0.1:5678')
loop.run_forever()
loop.close()